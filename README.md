
### Installation and Configuration

##### Execute these commands below, in order

~~~
1. git clone https://gitlab.com/ideal1386/shop-cms.git
~~~

~~~
1. composer install
~~~

~~~
2. cp .env-example .env
~~~
~~~
4. php artisan migrate --seed
~~~

after that you must run the command to work queue
~~~
5. php artisan queue:work
~~~

**How to log in as admin:**

> *http(s)://example.com/admin*

~~~
username:idealit
password:I@deal/123+
~~~

