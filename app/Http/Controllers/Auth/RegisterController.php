<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\Sms\UserCreated;
use App\Notifications\UserRegistered;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'regex:/(09)[0-9]{9}/', 'digits:11', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed:confirmed'],
            'captcha' => ['required', 'captcha'],
        ], [
            'username.required' => 'لطفا یک شماره موبایل معتبر وارد کنید',
            'username.string' => 'لطفا یک شماره موبایل معتبر وارد کنید',
            'username.regex' => 'لطفا یک شماره موبایل معتبر وارد کنید',
            'username.digits' => 'لطفا یک شماره موبایل معتبر وارد کنید',
            'username.unique' => 'شماره موبایل وارد شده تکراری است',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
        ]);

        // send notification for admins
        $admins = User::where('level', 'admin')->get();
        Notification::send($admins, new UserRegistered($user));

        // send sms notification to user
        Notification::send($user, new UserCreated($user));

        return $user;
    }

    public function showRegistrationForm()
    {
        $view = config('front.pages.register');

        if (!$view) {
            abort(404);
        }

        return view($view);
    }
}
