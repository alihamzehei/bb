<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class DeveloperController extends Controller
{
    public function showSettings()
    {
        $schedule_last_work = option('schedule_run');
        $schedule_run = false;

        if ($schedule_last_work) {
            $schedule_last_work = Carbon::createFromDate($schedule_last_work);
            $diff = $schedule_last_work->diffInMinutes(now());
            $schedule_run = ($diff <= 2);
        }

        return view('back.developer.settings', compact('schedule_run'));
    }

    public function updateSettings(Request $request)
    {
        $developer_options = $request->all();

        foreach ($developer_options as $option => $value) {
            option_update($option, $value);
        }

        if ($request->app_debug_mode) {
            option_update('app_debug_mode', 'on');
        } else {
            option_update('app_debug_mode', 'off');
        }

        if ($request->debugbar_enabled) {
            change_env('DEBUGBAR_ENABLED', 'true');
        } else {
            change_env('DEBUGBAR_ENABLED', 'false');
        }

        return response('success');
    }

    public function downApplication(Request $request)
    {
        $request->validate([
            'secret' => 'required|string'
        ]);

        Artisan::call("down --render='errors::503' --secret='$request->secret'");

        return response()->json(['secret' => $request->secret]);
    }

    public function upApplication()
    {
        Artisan::call("up");

        return response('success');
    }
}
