<?php

namespace App\Http\Controllers\Back;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Post::class, 'post');
    }

    public function index()
    {
        $posts = Post::latest()->paginate(10);

        return view('back.posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::where('type', 'postcat')->orderBy('ordering')->get();

        return view('back.posts.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
            'slug'        => 'nullable|unique:posts,slug',
            'publish_date' => 'nullable|date',
        ]);

        $post = Post::create([
            'title'              => $request->title,
            'content'            => $request->content,
            'category_id'        => $request->category_id,
            'published'          => $request->published ? true : false,
            'slug'               => $request->slug ?: $request->title,
            'meta_title'         => $request->meta_title,
            'meta_description'   => $request->meta_description,
            'publish_date'       => $request->publish_date ? Verta::parse($request->publish_date)->datetime() : null,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $post->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('posts', $name);

            $post->image = '/uploads/posts/' . $name;
            $post->save();
        }

        toastr()->success('نوشته با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    public function edit(Post $post)
    {
        $categories = Category::where('type', 'postcat')->orderBy('ordering')->get();

        return view('back.posts.edit', compact('post', 'categories'));
    }

    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title'       => 'required|string|max:191',
            'category_id' => 'required|exists:categories,id',
            'image'       => 'image',
            'slug'        => "nullable|unique:products,slug,$post->id",
            'publish_date' => 'nullable|date',
        ]);

        $post->update([
            'title'              => $request->title,
            'content'            => $request->content,
            'category_id'        => $request->category_id,
            'published'          => $request->published ? true : false,
            'slug'               => $request->slug ?: $request->title,
            'meta_title'         => $request->meta_title,
            'meta_description'   => $request->meta_description,
            'publish_date'       => $request->publish_date ? Verta::parse($request->publish_date)->datetime() : null,
        ]);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $name = uniqid() . '_' . $post->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('posts', $name);

            Storage::disk('public')->delete($post->image);
            $post->image = '/uploads/posts/' . $name;
            $post->save();
        }

        toastr()->success('نوشته با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    public function destroy(Post $post)
    {
        Storage::disk('public')->delete($post->image);
        $post->delete();
    }

    public function generate_slug(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $slug = SlugService::createSlug(Post::class, 'slug', $request->title);

        return response()->json(['slug' => $slug]);
    }

    //------------- Category methods

    public function categories()
    {
        $this->authorize('posts.category');

        $categories = Category::where('type', 'postcat')->whereNull('category_id')
            ->with('childrenCategories')
            ->orderBy('ordering')
            ->get();

        return view('back.posts.categories', compact('categories'));
    }
}
