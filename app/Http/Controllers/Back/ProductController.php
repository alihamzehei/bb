<?php

namespace App\Http\Controllers\Back;

use App\Models\AttributeGroup;
use App\Models\Brand;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Models\PriceChange;
use App\Models\Product;
use App\Models\Specification;
use App\Models\SpecificationGroup;
use App\Models\SpecType;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Product::class, 'product');
    }

    public function index(Request $request)
    {
        $products   = Product::filter($request)->customPaginate($request);

        return view('back.products.index', compact('products'));
    }

    public function indexPrices(Request $request)
    {
        $this->authorize('products.prices');

        $products   = Product::filter($request)->customPaginate($request);

        return view('back.products.prices', compact('products'));
    }

    public function updatePrices(Request $request)
    {
        $this->authorize('products.prices');

        $request->validate([
            'products'        => 'required|array',
        ]);

        foreach ($request->products as $key => $value) {
            $product = Product::find($key);

            if (!$product) {
                continue;
            }


            foreach ($product->prices as $price) {
                if (!isset($value['prices'][$price->id])) {
                    continue;
                }

                $request_price = $value['prices'][$price->id];
                if (isset($request_price['price']) && isset($request_price['stock']) && ($request_price['price'] != $price->price || $request_price['stock'] != $price->stock)) {

                    if ($request_price['price'] != $price->price) {
                        PriceChange::create([
                            'product_id' => $product->id,
                            'price_id'   => $price->id,
                            'price'      => $request_price['price'],
                            'discount'   => $price->discount
                        ]);
                    }

                    $price->update([
                        'price' => $request_price['price'],
                        'stock' => $request_price['stock'],
                    ]);
                }
            }
        }

        return response('success');
    }

    public function store(StoreProduct $request)
    {
        $product = Product::create([
            'title'              => $request->title,
            'title_en'           => $request->title_en,
            'type'               => $request->type,
            'category_id'        => $request->category_id,
            'spec_type_id'       => spec_type($request),
            'weight'             => $request->weight,
            'price_type'         => "multiple-price",
            'description'        => $request->description,
            'short_description'  => $request->short_description,
            'special'            => $request->special ? true : false,
            'slug'               => $request->slug ?: $request->title,
            'meta_title'         => $request->meta_title,
            'image_alt'          => $request->image_alt,
            'meta_description'   => $request->meta_description,
            'published'          => $request->published ? true : false,
        ]);

        // update product brand
        $this->updateProductBrand($product, $request);

        // update product prices
        $this->updateProductPrices($product, $request);

        // update product specifications
        $this->updateProductSpecifications($product, $request);

        // update product images
        $this->updateProductImages($product, $request);

        toastr()->success('محصول با موفقیت ایجاد شد.');

        return response("success", 200);
    }

    public function create(Request $request)
    {
        $categories      = Category::where('type', 'productcat')->orderBy('ordering')->get();
        $specTypes       = SpecType::all();
        $attributeGroups = AttributeGroup::orderBy('ordering')->get();

        $copy_product = $request->product ? Product::where('slug', $request->product)->first() : null;

        return view('back.products.create', compact(
            'categories',
            'specTypes',
            'attributeGroups',
            'copy_product'
        ));
    }

    public function edit(Product $product)
    {
        $categories =  Category::where('type', 'productcat')->orderBy('ordering')->get();
        $specTypes  = SpecType::all();
        $attributeGroups = AttributeGroup::orderBy('ordering')->get();

        return view('back.products.edit', compact(
            'product',
            'categories',
            'specTypes',
            'attributeGroups'
        ));
    }

    public function update(UpdateProduct $request, Product $product)
    {
        if ($product->price_type != $request->price_type) {
            DB::table('cart_product')->where('product_id', $product->id)->delete();
        }

        $product->update([
            'title'              => $request->title,
            'title_en'           => $request->title_en,
            'category_id'        => $request->category_id,
            'spec_type_id'       => spec_type($request),
            'weight'             => $request->weight,
            'price_type'         => "multiple-price",
            'description'        => $request->description,
            'short_description'  => $request->short_description,
            'special'            => $request->special ? true : false,
            'slug'               => $request->slug ?: $request->title,
            'meta_title'         => $request->meta_title,
            'image_alt'          => $request->image_alt,
            'meta_description'   => $request->meta_description,
            'published'          => $request->published,
            'publish_date'       => $request->publish_date ? Verta::parse($request->publish_date)->datetime() : null,
        ]);

        // update product brand
        $this->updateProductBrand($product, $request);

        // update product prices
        $this->updateProductPrices($product, $request);

        // update product specifications
        $this->updateProductSpecifications($product, $request);

        // update product images
        $this->updateProductImages($product, $request);

        toastr()->success('محصول با موفقیت ویرایش شد.');

        return response("success", 200);
    }

    public function image_store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|image|mimes:jpeg,png,jpg|max:10240',
        ]);

        $image = $request->file('file');

        $currentDate = Carbon::now()->toDateString();
        $imagename = 'img' . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

        $image->storeAs('tmp', $imagename);

        return response()->json(['imagename' => $imagename]);
    }

    public function image_delete(Request $request)
    {
        $filename = $request->get('filename');

        if (Storage::exists('tmp/' . $filename)) {
            Storage::delete('tmp/' . $filename);
        }

        return response('success');
    }

    public function destroy(Product $product)
    {
        $product->tags()->detach();
        $product->specifications()->detach();

        if ($product->image && Storage::disk('public')->exists($product->image)) {
            Storage::disk('public')->delete($product->image);
        }

        foreach ($product->gallery as $image) {
            if (Storage::disk('public')->exists($image->image)) {
                Storage::disk('public')->delete($image->image);
            }

            $image->delete();
        }

        $product->delete();

        return response('success');
    }

    public function generate_slug(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $slug = SlugService::createSlug(Product::class, 'slug', $request->title);

        return response()->json(['slug' => $slug]);
    }

    //------------- Category methods

    public function categories()
    {
        $this->authorize('products.category');

        $categories = Category::where('type', 'productcat')->whereNull('category_id')
            ->with('childrenCategories')
            ->orderBy('ordering')
            ->get();

        return view('back.products.categories', compact('categories'));
    }

    private function updateProductPrices(Product $product, Request $request)
    {
        $prices_id = [];

        foreach ($request->prices as $price) {

            $attributes = array_filter($price['attributes']);

            $update_price = false;

            foreach ($product->prices()->withTrashed()->get() as $product_price) {
                $product_price_attributes = $product_price->get_attributes()->get()->pluck('id')->toArray();

                sort($attributes);
                sort($product_price_attributes);

                if ($attributes == $product_price_attributes) {
                    $update_price = $product_price;
                    break 1;
                }
            }

            if ($update_price) {

                if (($update_price->price != $price["price"]) || $update_price->discount != $price["discount"]) {
                    PriceChange::create([
                        'price_id'   => $update_price->id,
                        'product_id' => $product->id,
                        'price'      => $price["price"],
                        'discount'   => $price["discount"]
                    ]);
                }

                $update_price->update([
                    "price"       => $price["price"],
                    "discount"    => $price["discount"],
                    "stock"       => $price["stock"],
                    "cart_max"    => $price["cart_max"],
                    "cart_min"    => $price["cart_min"],
                    "deleted_at"  => null,
                ]);

                $update_price->get_attributes()->sync($attributes);

                $prices_id[] = $update_price->id;
            } else {

                $insert_price = $product->prices()->create(
                    [
                        "price"       => $price["price"],
                        "discount"    => $price["discount"],
                        "stock"       => $price["stock"],
                        "cart_max"    => $price["cart_max"],
                        "cart_min"    => $price["cart_min"],
                    ]
                );

                foreach ($attributes as $attribute) {
                    $insert_price->get_attributes()->attach([$attribute]);
                }

                PriceChange::create([
                    'price_id'   => $insert_price->id,
                    'product_id' => $product->id,
                    'price'      => $price["price"],
                    'discount'   => $price["discount"]
                ]);

                $prices_id[] = $insert_price->id;
            }
        }

        $product->prices()->whereNotIn('id', $prices_id)->delete();
    }

    private function updateProductSpecifications(Product $product, Request $request)
    {
        $product->specifications()->detach();
        $group_ordering = 0;

        if ($request->specification_group) {
            foreach ($request->specification_group as $group) {

                if (!isset($group['specifications'])) {
                    continue;
                }

                $spec_group = SpecificationGroup::firstOrCreate([
                    'name' => $group['name'],
                ]);

                $specification_ordering = 0;

                foreach ($group['specifications'] as $specification) {
                    $spec = Specification::firstOrCreate([
                        'name' => $specification['name']
                    ]);

                    $product->specifications()->attach([
                        $spec->id => [
                            'specification_group_id' => $spec_group->id,
                            'group_ordering'         => $group_ordering,
                            'specification_ordering' => $specification_ordering++,
                            'value'                  => $specification['value'],
                            'special'                => isset($specification['special']) ? true : false
                        ]
                    ]);
                }

                $group_ordering++;
            }
        }
    }

    private function updateProductBrand(Product $product, Request $request)
    {
        if ($request->brand) {
            $brand = Brand::firstOrCreate(
                [
                    'name' => $request->brand,
                ]
            );

            $product->update([
                'brand_id' => $brand->id
            ]);
        }
    }

    private function updateProductImages(Product $product, Request $request)
    {
        if ($request->hasFile('image')) {
            if ($product->image && Storage::disk('public')->exists($product->image)) {
                Storage::disk('public')->delete($product->image);
            }

            $file = $request->image;
            $name = uniqid() . '_' . $product->id . '.' . $file->getClientOriginalExtension();
            $request->image->storeAs('products', $name);

            $product->image = '/uploads/products/' . $name;
            $product->save();
        }

        $product_images = $product->gallery()->pluck('image')->toArray();
        $images         = explode(',', $request->images);
        $deleted_images = array_diff($product_images, $images);

        foreach ($deleted_images as $del_img) {
            $del_img = $product->gallery()->where('image', $del_img)->first();

            if (!$del_img) {
                continue;
            }

            if (Storage::disk('public')->exists($del_img)) {
                Storage::disk('public')->delete($del_img);
            }

            $del_img->delete();
        }

        $ordering = 1;

        if ($request->images) {

            foreach ($images as $image) {

                if (Storage::exists('tmp/' . $image)) {

                    Storage::move('tmp/' . $image, 'products/' . $image);

                    $product->gallery()->create([
                        'image'    => '/uploads/products/' . $image,
                        'ordering' => $ordering++,
                    ]);
                } else {
                    $product->gallery()->where('image', $image)->update([
                        'ordering' => $ordering++,
                    ]);
                }
            }
        }
    }
}
