<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    public function showInformation()
    {
        $this->authorize('settings');

        $provinces = Province::all();

        $info_province = option('info_province_id');

        if ($info_province) {
            $cities = Province::find($info_province)->cities;
        } else {
            $cities = [];
        }

        return view('back.settings.information', compact('provinces', 'cities'));
    }

    public function updateInformation(Request $request)
    {
        $this->authorize('settings.information');

        $informations = $request->except(['info_icon', 'info_logo']);

        $this->validate($request, [
            'info_site_title' => 'required',
            'info_icon' => 'image|max:2048',
            'info_logo' => 'image|max:2048',
            'info_city_id' => 'exists:cities,id',
            'info_province_id' => 'exists:provinces,id',
        ]);

        if ($request->hasFile('info_icon')) {

            $imageName = time() . '_icon.' . $request->info_icon->getClientOriginalExtension();
            $request->info_icon->move(public_path('uploads/'), $imageName);

            $old_icon = option('info_icon');

            if ($old_icon) {
                Storage::disk('public')->delete($old_icon);
            }

            $informations['info_icon'] = '/uploads/' . $imageName;
        }

        if ($request->hasFile('info_logo')) {

            $imageName = time() . '_logo.' . $request->info_logo->getClientOriginalExtension();
            $request->info_logo->move(public_path('uploads/'), $imageName);

            $old_logo = option('info_logo');

            if ($old_logo) {
                Storage::disk('public')->delete($old_logo);
            }

            $informations['info_logo'] = '/uploads/' . $imageName;
        }

        $admin_route_prefix_changed = option('admin_route_prefix') != $request->admin_route_prefix ? true : false;
        $admin_route_prefix = $request->admin_route_prefix;

        foreach ($informations as $information => $value) {
            option_update($information, $value);
        }

        return response()->json([
            'admin_route_prefix'         => $admin_route_prefix,
            'admin_route_prefix_changed' => $admin_route_prefix_changed
        ]);
    }

    public function showSocials()
    {
        return view('back.settings.socials');
    }

    public function updateSocials(Request $request)
    {
        $this->authorize('settings.socials');

        $socials = $request->all();

        foreach ($socials as $social => $value) {
            option_update($social, $value);
        }

        return response('success');
    }

    public function showGateways()
    {
        return view('back.settings.gateways');
    }

    public function updateGateways(Request $request)
    {
        $this->authorize('settings.gateway');

        $gateways = $request->except(['mellat', 'payir']);

        if ($request->payir) {
            $request->validate([
                'gateway_payir_api' => 'required',
            ]);

            option_update('gateway_payir_status', 'on');
        } else {
            option_update('gateway_payir_status', 'off');
        }

        if ($request->mellat) {
            $request->validate([
                'gateway_mellat_username'   => 'required',
                'gateway_mellat_password'   => 'required',
                'gateway_mellat_terminalId' => 'required',
            ]);

            option_update('gateway_mellat_status', 'on');
        } else {
            option_update('gateway_mellat_status', 'off');
        }

        foreach ($gateways as $gateway => $value) {
            option_update($gateway, $value);
        }

        return response('success');
    }

    public function showOthers()
    {
        return view('back.settings.others');
    }

    public function updateOthers(Request $request)
    {
        $this->authorize('settings.others');

        $env_options = [
            'PUSHER_APP_ID',
            'PUSHER_APP_KEY',
            'PUSHER_APP_SECRET',
            'PUSHER_APP_CLUSTER',
        ];

        $env = $request->only($env_options);

        foreach ($env as $key => $value) {
            change_env($key, $value);
        }

        $others = $request->except($env_options);

        foreach ($others as $social => $value) {
            option_update($social, $value);
        }

        return response('success');
    }

    public function showThemeSettings()
    {
        return view('back.settings.themeSettings');
    }

    public function updateThemeSettings(Request $request)
    {
        $settings = $request->all();

        foreach ($settings as $setting => $value) {
            option_update($setting, $value);
        }

        return response('success');
    }
}
