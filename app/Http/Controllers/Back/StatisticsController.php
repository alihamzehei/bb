<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Viewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function views()
    {
        $views = Viewer::latest();

        if (auth()->user()->level != 'creator') {
            $views = $views->whereNull('user_id')->orWhere(function ($query) {
                $query->whereHas('user', function ($q1) {
                    $q1->where('level', '!=', 'creator');
                });
            });
        }

        $views = $views->paginate(20);

        return view('back.reports.views', compact('views'));
    }

    public function viewers()
    {
        $viewers = Viewer::latest()->whereDate('created_at', now())->get()->unique('user_id');

        return view('back.reports.viewers', compact('viewers'));
    }
}
