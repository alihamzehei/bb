<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Transaction;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Transaction::class, 'transaction');
    }

    public function index()
    {
        $transactions = Transaction::latest()->paginate(20);

        return view('back.transactions.index', compact('transactions'));
    }

    public function show(Transaction $transaction)
    {
        return view('back.transactions.show', compact('transaction'))->render();
    }

    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return response('success');
    }
}
