<?php

namespace App\Http\Controllers;

use App\Models\Province;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    public function getCities(Request $request) {
        $province = Province::findOrFail($request->id);
        return response()->json($province->cities()->get(['id', 'name']));
    }
}
