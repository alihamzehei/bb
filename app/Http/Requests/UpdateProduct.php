<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();

        $rules = [
            'product_id'   => 'required|exists:products,id',
            'title'        => 'required|string|max:191',
            'title_en'     => 'nullable|string|max:191',
            'category_id'  => 'required|exists:categories,id',
            'discount'     => 'max:100|min:0',
            'image'        => 'image',
            'slug'         => "nullable|unique:products,slug,$request->product_id",
            'publish_date' => 'nullable|date',
            'weight'       => 'required|integer',
        ];

        $rules = array_merge($rules, [
            'prices'                => 'required|array',
            'prices.*.price'        => 'required|integer|min:100|max:50000000',
            'prices.*.stock'        => 'required|integer',
            'prices.*.attributes'   => "required|array",
            'prices.*.attributes.*' => "nullable|exists:attributes,id",
            'prices.*.cart_max'     => 'nullable|integer',
        ]);

        return $rules;
    }
}
