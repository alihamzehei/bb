<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'order_id'     => $this->id,
            'name'         => $this->name,
            'created_at'   => verta($this->created_at)->format('%Y - %m - %d'),
            'price'        => $this->price,
            'status'       => $this->status,

            'links' => [
                'view'    => route('admin.orders.show', ['order' => $this]),
                // 'edit'    => route('forms.edit', ['form' => $this]),
                // 'destroy' => route('forms.destroy', ['form' => $this]),
            ]
        ];
    }
}
