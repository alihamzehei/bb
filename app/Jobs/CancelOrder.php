<?php

namespace App\Jobs;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CancelOrder implements
    ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->order && $this->order->status == 'unpaid') {
            $this->order->update([
                'status' => 'canceled',
            ]);

            foreach ($this->order->items as $item) {
                if ($item->product) {

                    $query = $item->product->prices()->where('price_id', $item->price_id);
                    $query->update([
                        'stock' => ($query->first() ? $query->first()->stock : 0) + $item->quantity
                    ]);
                }
            }
        }
    }
}
