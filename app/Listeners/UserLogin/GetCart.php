<?php

namespace App\Listeners\UserLogin;

use App\Models\Cart;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class GetCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $cart_id = Cookie::get('cart_id');

        if ($cart_id) {
            $cart = Cart::find($cart_id);

            if ($cart && $cart->user_id == null) {

                $user = $event->user;

                $user_cart = Cart::where('user_id', $user->id)->first();

                //remove download product from cart if user hasBought that product

                foreach ($cart->products as $product) {
                    if ($product->type == 'download' && $user->hasBought($product)) {
                        $cart->products()->detach($product->id);
                    }
                }

                if (!$user_cart) {
                    $cart->update([
                        'user_id' => $user->id,
                    ]);
                } else {
                    foreach ($cart->products as $product) {
                        $query = DB::table('cart_product')->where('cart_id', $user_cart->id)->where('product_id', $product->id)->where('price_id', $product->pivot->price_id);
                        $user_cart_product = $query->first();

                        if (!$user_cart_product) {

                            DB::table('cart_product')->insert([
                                'cart_id'    => $user_cart->id,
                                'product_id' => $product->id,
                                'quantity'   => $product->pivot->quantity,
                                'price_id'   => $product->pivot->price_id,
                            ]);

                        } else {

                            $query->update([
                                'quantity' => $product->pivot->quantity,
                            ]);
                        }
                    }

                    $cart->delete();
                }

                Cookie::queue(Cookie::forget('cart_id'));
            }
        }
    }
}
