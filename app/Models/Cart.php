<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = ['id'];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(['quantity', 'price_id', 'id'])->withTimestamps();
    }

    public function getPriceAttribute()
    {
        $price = 0;

        foreach ($this->products as $product) {
            $price += ($product->prices()->find($product->pivot->price_id)->price * $product->pivot->quantity);
        }

        return $price;
    }

    public function discountPrice()
    {
        $price = 0;

        foreach ($this->products as $product) {
            $product_price = $product->prices()->find($product->pivot->price_id);
            $price += (get_discount_price($product_price->price, $product_price->discount) * $product->pivot->quantity);
        }

        return $price;
    }

    public function finalPrice($city_id = null)
    {
        return $this->discountPrice() + $this->shipping_cost($city_id, true);
    }

    public function getQuantityAttribute()
    {
        $quantity = 0;

        foreach ($this->products as $product) {
            $quantity += $product->pivot->quantity;
        }

        return $quantity;
    }

    public function shipping_cost($city_id = null, $integer = false)
    {
        if ($city_id) {
            $city_cost = ShippingCost::where('city_id', $city_id)->where('price', '<=', $this->discountPrice())->orderBy('price', 'desc')->first();

            $shipping_cost =  $city_cost ? $city_cost->cost : option('info_shipping_cost', 10000);

            return $integer ? $shipping_cost : number_format($shipping_cost) . ' تومان';
        }

        return $integer ? 0 : 'وابسته به آدرس';
    }

    public function totalDiscount()
    {
        $discount = 0;

        foreach ($this->products as $product) {
            $product_price = $product->prices()->find($product->pivot->price_id);

            $discount += ($product_price->price * $product->pivot->quantity) - (get_discount_price($product_price->price, $product_price->discount) * $product->pivot->quantity);
        }

        return $discount;
    }
}
