<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'transactionable');
    }

    public function hasPhysicalItem()
    {
        return $this->products()->where('type', 'physical')->first() ? true : false;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_items');
    }

    public function getShipStatusAttribute()
    {
        if ($this->hasPhysicalItem()) {

            if ($this->status != 'paid') {
                return 'منتظر پرداخت';
            }

            $text = '';

            switch ($this->shipping_status) {
                case 'pending': {
                        $text = 'در حال بررسی';
                        break;
                    }
                case 'wating': {
                        $text = 'منتظر ارسال';
                        break;
                    }
                case 'sent': {
                        $text = 'ارسال شد';
                        break;
                    }
                case 'canceled': {
                        $text = 'ارسال لغو شد';
                        break;
                    }
            }
            return $text;
        }

        return 'سفارش شما شامل محصول فیزیکی نمی باشد';
    }

    public function scopeFilter($query, $request)
    {
        if (request('query')) {

            if (isset(request('query')['search']) && request('query')['search']) {
                $search = request('query')['search'];
                $query->where('first_name', 'like', '%' . $search . '%')->orWhere('last_name', 'like', '%' . $search . '%');
            }

            if (isset(request('query')['status']) && request('query')['status']) {
                $query->where('status', request('query')['status']);
            }
        }

        if ($request->sort && $this->getConnection()->getSchemaBuilder()->hasColumn($this->getTable(), $request->sort['field'])) {
            $query->orderBy($request->sort['field'], $request->sort['sort']);
        }

        return $query;
    }
}
