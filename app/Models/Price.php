<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function get_attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    public function getAttributesName()
    {
        $title = '';

        foreach ($this->get_attributes as $attribute) {
            $title .= ' ' . $attribute->group->name . ' : ' . $attribute->name . '،';
        }

        return $title;
    }
}
