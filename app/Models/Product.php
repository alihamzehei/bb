<?php

namespace App\Models;

use App\Traits\ProductScopes;
use App\Traits\Taggable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Product extends Model
{
    use HasFactory, sluggable, Taggable, ProductScopes;

    protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug',
            ],
        ];
    }

    //------------- start relations

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function gallery()
    {
        return $this->morphMany(Gallery::class, 'galleryable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function specificationGroups()
    {
        return $this->belongsToMany(SpecificationGroup::class, 'product_specification')->withPivot(['specification_group_id', 'group_ordering', 'specification_ordering', 'value', 'special'])->withTimestamps()->orderBy('group_ordering');
    }

    public function specifications()
    {
        return $this->belongsToMany(Specification::class, 'product_specification')->withPivot(['specification_group_id', 'group_ordering', 'specification_ordering', 'value', 'special'])->withTimestamps()->orderBy('specification_ordering');
    }

    public function specialSpecifications()
    {
        return $this->specifications()->where('special', true)->get();
    }

    public function specType()
    {
        return $this->belongsTo(SpecType::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class)->orderBy('price');
    }

    public function getPrices()
    {
        return $this->hasMany(Price::class)
            ->where('stock', '>', 0)
            ->select('*')
            ->addSelect(DB::raw('IF(discount!=0, price- (price * (discount / 100)), price ) AS current_price'))
            ->orderBy('current_price');
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function priceChanges()
    {
        return $this->hasMany(PriceChange::class);
    }

    //------------- end relations

    public function getDiscountPriceAttribute()
    {
        if ($this->discount) {
            return $this->price - ($this->price * ($this->discount / 100));
        }

        return $this->price;
    }

    public function download()
    {
        $time = Carbon::now()->addHours(5)->getTimestamp();

        $hash = Hash::make('gw23uy453@$#!#%$jf' . $time . $this->id);

        $link = route('front.products.download', ['product' => $this]);

        $link .= "?mac=$hash&time=$time";

        return $link;
    }

    public function link()
    {
        return route('front.products.show', ['product' => $this]);
    }

    public function addableToCart()
    {
        if ($this->type != 'physical') {
            return true;
        }

        if ($this->price_type == "multiple-price" && !$this->prices()->where('stock', '>', 0)->first()) {
            return false;
        }

        return true;
    }

    public function getLowestPrice($numeric = false)
    {
        $price = $this->getPrices()->first();

        if ($price && $price->stock) {
            return $numeric ? get_discount_price($price->price, $price->discount) : get_discount_price($price->price, $price->discount, true) . ' تومان';
        }

        return $numeric ? null : 'ناموجود';
    }

    public function getLowestDiscount()
    {
        $price = $this->getPrices()->first();

        if ($price && $price->discount) {
            return $price->price . ' تومان';
        }

        return null;
    }

    public function get_attributes($attributeGroup, $prev_attribute, $groups, $attributes_id)
    {
        $prices = $this->getPrices()->pluck('id');

        $group_attributes = $attributeGroup->get_attributes()->pluck('id');
        $attributes       = DB::table('attribute_price')->whereIn('price_id', $prices)->whereIn('attribute_id', $group_attributes);

        if ($groups) {
            $group_prices = $this->getPrices();

            foreach ($attributes_id as $att) {
                $group_prices->whereHas('get_attributes', function ($q) use ($att) {
                    $q->where('attribute_id', $att);
                });
            }

            $group_prices = $group_prices->pluck('id');

            $attributes->whereIn('price_id', $group_prices);
        }

        if ($prev_attribute) {
            $prices_have_this_attribute = $this->prices()->whereHas('get_attributes', function ($q) use ($prev_attribute) {
                $q->where('attribute_id', $prev_attribute->id);
            })->pluck('id');

            $this_price_attributes = DB::table('attribute_price')->whereIn('price_id', $prices_have_this_attribute)->pluck('attribute_id');

            $attributes->whereIn('attribute_id', $this_price_attributes);
        }

        $attributes = $attributes->pluck('attribute_id');

        if ($attributes->count()) {
            return Attribute::whereIn('id', $attributes)->get();
        }

        return null;
    }

    public function getPriceWithAttributes($attributes_id)
    {
        foreach ($this->getPrices as $price) {
            $price_attributes = $price->get_attributes()->pluck('attributes.id')->toArray();

            sort($price_attributes);
            sort($attributes_id);

            if ($price_attributes == $attributes_id) {
                return $price;
            }
        }
    }
}
