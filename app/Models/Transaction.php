<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id'];

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        switch ($this->transactionable_type) {
            case Order::class: {
                    $type = 'خرید محصول';
                    break;
                }
        }

        return $type;
    }

    public function link()
    {
        switch ($this->transactionable_type) {

            case Order::class: {
                    $link = route('admin.orders.show', ['order' => $this->transactionable]);
                    break;
                }
        }

        return $link;
    }
}
