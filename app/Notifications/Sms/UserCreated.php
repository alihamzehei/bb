<?php

namespace App\Notifications\Sms;

use App\Channels\SmsChannel;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class UserCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return [
            'pattern_code' => 'oagr8lmg75',
            'mobile'       => $notifiable->username,
            'input_data'   => [
                'fullname' => $notifiable->fullname
            ],
            'type'         => 'user-created'

        ];
    }
}
