<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Filter;
use App\Observers\ProductObserver;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use RachidLaasri\LaravelInstaller\Helpers\EnvironmentManager;
use Illuminate\Support\Facades\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (application_installed()) {

            if (!$this->app->runningInConsole()) {
                $this->viewComposer();
                $this->configs();
                // asset version used in v_asset() helper function
                $GLOBALS['asset_version'] = option('assets_version', 1);
            }

            $this->loadTheme();
        } else if (!application_installed() && !$this->app->runningInConsole()) {
            $this->app->bind(EnvironmentManager::class, MyEnvironmentManager::class);

            if (request()->segment(1) != 'install') {
                return redirect('install')->send();
            }
        }

        $this->observers();
    }

    private function viewComposer()
    {
        // SHARE WITH SPECIFIC VIEW

        view()->composer(['back.partials.notifications', 'back.partials.sidebar'], function ($view) {

            $notifications = auth()->user()->unreadNotifications;

            $view->with('notifications', $notifications);
        });

        view()->composer(['back.products.partials.filters'], function ($view) {

            $categories = Category::where('type', 'productcat')->orderBy('ordering')->get();

            $view->with('categories', $categories);
        });

        view()->composer(['back.products.categories.edit'], function ($view) {

            $filters = Filter::latest()->get();

            $view->with('filters', $filters);
        });

        view()->composer(['back.menus.index', 'back.sliders.create', 'back.sliders.edit', 'back.banners.create', 'back.banners.edit', 'back.links.create', 'back.links.edit'], function ($view) {

            $pages = Page::pluck('slug');

            $view->with('pages', $pages);
        });
    }

    private function configs()
    {
        $gateways = [
            //------------ Zarinpal gateway
            'zarinpal' => [
                'merchant-id'  => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                'type'         => 'zarin-gate',             // Types: [zarin-gate || normal]
                'callback-url' => env('APP_URL') . '/orders/payment/callback',
                'server'       => 'germany',                // Servers: [germany || iran || test]
                'email'        => 'email@gmail.com',
                'mobile'       => '09xxxxxxxxx',
                'description'  => 'description',
            ],

            //------------ Mellat gateway
            'mellat' => [
                'username'     => option('gateway_mellat_username'),
                'password'     => option('gateway_mellat_password'),
                'terminalId'   => option('gateway_mellat_terminalId'),
                'callback-url' => env('APP_URL') . '/orders/payment/mellat/callback'
            ],

            //------------ PayIr gateway
            'payir'    => [
                'api'          => option('gateway_payir_api', 'test'),
                'callback-url' => env('APP_URL') . '/orders/payment/callback'
            ],
        ];

        foreach ($gateways as $key => $values) {
            foreach ($values as $k => $val) {
                Config::set("gateway.$key.$k", $val);
            }
        }

        Config::set("general.admin_route_prefix", option('admin_route_prefix'));

        Config::set("app.debug", option('app_debug_mode') == 'on' ?: false);

        verta()->timezone = 'Asia/Tehran';
    }

    private function observers()
    {
        Product::observe(ProductObserver::class);
    }

    public static function loadTheme()
    {
        // register theme service provider

        $theme = get_current_theme();

        if ($theme && class_exists($theme['service_provider'])) {
            app()->register($theme['service_provider']);
        }
    }
}
