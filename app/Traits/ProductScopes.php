<?php

namespace App\Traits;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait ProductScopes
{
    public function scopePublished($query)
    {
        $query->where('published', true)->where(function ($q) {
            $q->where('publish_date', null)->orWhere('publish_date', '<=', Carbon::now());
        });

        return $query;
    }

    public function scopeAvailable($query)
    {
        $query->where(function ($query2) {
            $query2->where('price_type', 'multiple-price')->whereHas('prices', function ($p) {
                $p->where('stock', '>', 0);
            });
        });

        return $query;
    }

    public function scopeUnavailable($query)
    {
        $query->where(function ($query2) {
            $query2->where('price_type', 'multiple-price')->whereDoesntHave('prices', function ($p) {
                $p->where('stock', '>', 0);
            });
        });

        return $query;
    }

    public function scopeDiscount($query)
    {
        $query->where(function ($query2) {
            $query2->where('price_type', 'multiple-price')->whereHas('prices', function ($p) {
                $p->where('discount', '>', 0);
            });
        });

        return $query;
    }

    public function scopeCustomPaginate($query, $request)
    {
        $paginate = $request->paginate;
        $paginate = ($paginate && is_numeric($paginate)) ? $paginate : 10;

        if ($request->paginate == 'all') {
            $paginate = $query->count();
        }

        return $query->paginate($paginate);
    }

    public function scopeOrderByStock($query)
    {
        $query
            ->select('*')
            ->selectRaw(DB::raw('if ((select max(stock) from prices where prices.product_id = products.id and prices.deleted_at is null) > 0, 1, 0) as in_stock'))
            ->orderBy('in_stock', 'desc');

        return $query;
    }

    public function scopeFilter($query, $request)
    {
        if ($request->title) {
            $query->where('title', 'like', '%' . $request->title . '%');
        }

        if ($request->special) {
            $query->where('special', true);
        }

        if ($request->stock) {
            if ($request->stock == 'available') {

                $query->available();
            } else if ($request->stock == 'unavailable') {
                $query->unavailable();
            }
        }

        if ($request->published) {
            if ($request->published == 'yes') {

                $query->where('published', true);
            } else if ($request->published == 'no') {
                $query->where('published', false);
            }
        }

        $categories = $request->category_id;

        if ($categories) {

            $allcats = $categories;

            foreach ($categories as $category) {

                $category = Category::find($category);

                if ($category) {
                    $allcats = array_merge($category->allChildCategories(), $allcats);
                }
            }

            $query->whereIn('category_id', $allcats);
        }

        switch ($request->ordering) {
            case 'oldest': {
                    $query->oldest();
                    break;
                }
            default: {
                    $query->latest();
                }
        }

        return $query;
    }

    public function scopeFrontFilter($query, $category)
    {
        $request = request();

        $attribute_groups = [];

        if ($request->filters) {
            foreach ($request->filters as $key => $values) {
                $filter = $category->getFilter()->related()->find($key);

                if ($filter && is_array($values)) {

                    $values = array_keys($values);

                    switch ($filter->filterable_type) {
                        case "App\Models\AttributeGroup": {
                                $vals = array_values($values);
                                $attribute_groups = array_merge($attribute_groups, $vals);

                                $query->whereHas('prices', function ($q1) use ($values, $request) {
                                    $q1->whereHas('get_attributes', function ($q2) use ($values) {
                                        $q2->whereIn('attribute_id', $values);
                                    });

                                    if ($request->in_stock) {
                                        $q1->where('stock', '>', 0);
                                    }
                                });
                                break;
                            }
                        case "App\Models\Specification": {
                                $query->whereHas('specifications', function ($q3) use ($values, $filter) {
                                    if ($filter->separator) {
                                        $q3->where('product_specification.specification_id', $filter->filterable_id)->where(function ($q4) use ($values, $filter) {
                                            $count = 0;
                                            foreach ($values as $item) {
                                                if ($count == 0) {
                                                    $q4->where('value', 'like', '%' . $item . '%');
                                                    $count++;
                                                } else {
                                                    $q4->orWhere('value', 'REGEXP', '%' . $item . '%');
                                                }
                                            }
                                        });
                                    } else {
                                        $q3->where('product_specification.specification_id', $filter->filterable_id)->whereIn('value', $values);
                                    }
                                });

                                break;
                            }
                        case "App\Models\StaticFilter": {

                                switch ($filter->filterable->type) {
                                    case 'brand': {
                                            $query->whereIn('brand_id', $values);
                                            break;
                                        }
                                    case 'child_category': {
                                            $query->whereIn('category_id', $values);
                                            break;
                                        }
                                }

                                break;
                            }
                    }
                }
            }
        }

        if ($request->in_stock) {
            $query->available();
        }

        if ($request->price_filter && $request->min_price) {
            $query->whereHas('prices', function ($q) use ($request) {
                $q->where('stock', '>', 0)->where('price', '>=', $request->min_price);
            });
        }

        if ($request->price_filter && $request->max_price) {
            $query->whereHas('prices', function ($q) use ($request) {
                $q->where('stock', '>', 0)->where('price', '<=', $request->max_price);
            });
        }

        if ($request->s && is_string($request->s)) {
            $query->where(function ($query2) use ($request) {
                $query2->where('title', 'like', '%' . $request->s . '%')->orWhere('title_en', 'like', '%' . $request->s . '%');
            });
        }

        return $query;
    }
}
