<?php

// helper functions

use App\Models\Cart;
use App\Models\Category;
use App\Models\Option;
use App\Models\Product;
use App\Models\Sms;
use App\Models\Specification;
use App\Models\SpecificationGroup;
use App\Models\SpecType;
use App\Models\Tag;
use App\Models\Viewer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

/* add active class to li */

function active_class($route_name)
{

    $current_route = Request::route()->getName();

    return ($current_route == $route_name) ? 'active' : '';
}

function open_class($route_list)
{
    $text = '';

    $current_route = Request::route()->getName();

    foreach ($route_list as $route) {
        if ($current_route == $route) {
            $text = 'open';
            break;
        }
    }

    return $text;
}

function option_update($option_name, $option_value)
{
    $option = Option::firstOrNew(['option_name' => $option_name]);

    $option->option_value = $option_value;
    $option->save();
}

function option($option_name, $default_value = '')
{
    $option = Option::where('option_name', $option_name)->first();

    return $option ? $option->option_value : $default_value;
}

// add new tags and return tags id
function addTags($tags)
{
    $tags = explode(',', $tags);
    $tags_id = [];

    foreach ($tags as $item) {
        $tag = Tag::where('name', $item)->first();
        if (!$tag) {
            $tag = Tag::create([
                'name' => $item,
            ]);
        }
        $tags_id[] = $tag->id;
    }

    return $tags_id;
}

function get_cart()
{
    $cart = null;

    if (auth()->check()) {
        $cart = auth()->user()->cart;
    } else {
        $cart_id = Cookie::get('cart_id');

        if ($cart_id) {
            $cart = Cart::whereNull('user_id')->find($cart_id);
        }
    }

    return $cart;
}

/* return true if cart products quantity is ok
 * and return false if cart products quantity is more than product stock
 */
function check_cart_quantity()
{
    $cart = get_cart();

    if (!$cart || !$cart->products()->count()) {
        return true;
    }

    foreach ($cart->products as $product) {
        $price = $product->prices()->find($product->pivot->price_id);

        if ($product->pivot->quantity > $price->stock || ($price->cart_max !== null ? $product->pivot->quantity > $price->cart_max : false) || ($price->cart_min !== null ? $product->pivot->quantity < $price->cart_min : false)) {
            return false;
        }
    }

    return true;
}

//get user address
function user_address($key)
{
    if (old($key)) {
        return old($key);
    }

    return auth()->user()->address ? auth()->user()->address->$key : '';
}

function short_content($str, $words = 20, $strip_tags = true)
{
    if ($strip_tags) {
        $str = strip_tags($str);
    }

    return Str::words($str, $words);
}


function spec_type($request)
{
    if (!$request->spec_type || !$request->specification_group) {
        return null;
    }

    $spec_type = SpecType::firstOrCreate([
        'name' => $request->spec_type
    ]);

    $group_ordering = 0;

    foreach ($request->specification_group as $group) {

        if (!isset($group['specifications'])) {
            continue;
        }

        $spec_group = SpecificationGroup::firstOrCreate([
            'name' => $group['name'],
        ]);

        $specification_ordering = 0;

        foreach ($group['specifications'] as $specification) {
            $spec = Specification::firstOrCreate([
                'name' => $specification['name']
            ]);

            if (!$spec_type->specifications()->where('specification_id', $spec->id)->where('specification_group_id', $spec_group->id)->first()) {
                $spec_type->specifications()->attach([
                    $spec->id => [
                        'specification_group_id' => $spec_group->id,
                        'group_ordering'         => $group_ordering,
                        'specification_ordering' => $specification_ordering++,
                    ]
                ]);
            }
        }

        $group_ordering++;
    }

    return $spec_type->id;
}

function viewers_data($number = 7)
{
    $data = [];

    for ($i = 0; $i < $number; $i++) {
        $date = Carbon::now()->subDays($i);

        $views = Viewer::whereDate('created_at', $date)->count();

        $data[verta($date)->format('l')] = $views;
    }

    return $data;
}

function ip_data($number = 7)
{
    $data = [];

    for ($i = 0; $i < $number; $i++) {
        $date = Carbon::now()->subDays($i);

        $views = Viewer::whereDate('created_at', $date)->distinct('ip')->count();

        $data[verta($date)->format('l')] = $views;
    }

    return $data;
}

function array_to_string($array)
{
    $comma_separated = implode("','", $array);
    $comma_separated = "'" . $comma_separated . "'";
    return $comma_separated;
}

function get_discount_price($price, $discount, $number_format = false)
{
    $price = $price - ($price * ($discount / 100));

    return $number_format ? number_format($price) : $price;
}

function category_group($key)
{
    switch ($key) {
        case 'postcat': {
                return 'دسته بندی وبلاگ';
            }
        case 'productcat': {
                return 'دسته بندی محصول';
            }
    }
}

function v_asset($url, $version = null)
{
    if ($version) {
        $url .= '?v=' . $version;
    } else {
        $url .= '?v=';
        $url .= isset($GLOBALS['asset_version']) ? $GLOBALS['asset_version'] : '';
    }

    return asset($url);
}

function convert2english($string)
{
    $newNumbers = range(0, 9);
    // 1. Persian HTML decimal
    $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
    // 2. Arabic HTML decimal
    $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
    // 3. Arabic Numeric
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    // 4. Persian Numeric
    $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

    $string =  str_replace($persianDecimal, $newNumbers, $string);
    $string =  str_replace($arabicDecimal, $newNumbers, $string);
    $string =  str_replace($arabic, $newNumbers, $string);
    return str_replace($persian, $newNumbers, $string);
}

function carbon($string)
{
    return Carbon::createFromFormat('Y-m-d H:i:s', $string, 'Asia/Tehran')->timestamp;
}

function datatable($request, $query)
{
    $page = 1;

    if ($request->pagination && isset($request->pagination['page'])) {
        $page = $request->pagination['page'];
    }

    $columns = ['*'];
    $pageName = 'page';
    $perPage = 3;

    if ($request->pagination && isset($request->pagination['perpage'])) {
        $perPage = $request->pagination['perpage'];
    }

    if ($query->paginate($perPage, $columns, $pageName, $page)->lastPage() >= $page) {
        return $query->paginate($perPage, $columns, $pageName, $page);
    } else {
        return $query->paginate($perPage, $columns, $pageName, 1);
    }
}

/**
 * Send sms to specify pattern_code and mobile number with input data.
 *
 * @param  $pattern_code
 * @param  $mobile
 * @param  $input_data
 * @return $response
 */
function sendSms($pattern_code, $mobile, $input_data, $type = null)
{
    $username = env('SMS_PANEL_USERNAME');
    $password = env('SMS_PANEL_PASSWORD');
    $from = env('SMS_PANEL_FROM');
    $to = array($mobile);
    $url = "https://ippanel.com/patterns/pattern?username="
        . $username . "&password=" . urlencode($password)
        . "&from=$from&to=" . json_encode($to)
        . "&input_data=" . urlencode(json_encode($input_data))
        . "&pattern_code=$pattern_code";
    $handler = curl_init($url);
    curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($handler, CURLOPT_POSTFIELDS, $input_data);
    curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($handler);

    Sms::create([
        'mobile' => $mobile,
        'ip'     => request()->ip(),
        'type'   => $type
    ]);

    return $response;
}

function cart_min($selected_price)
{
    return $selected_price->cart_min ?: 1;
}

function cart_max($selected_price)
{
    return min($selected_price->cart_max, $selected_price->stock) ?: 1;
}

function get_category_products_id(Category $category)
{
    $allChildCategories = $category->allChildCategories();
    $product_ids = Product::whereIn('category_id', $allChildCategories)->pluck('id');
    return $product_ids;
}

function remove_id_from_url($id)
{
    $segments = request()->segments();

    if (($key = array_search($id, $segments)) !== false) {
        unset($segments[$key]);
    }

    return url(implode('/', $segments));
}

function get_separated_values($array, $separator)
{
    if (!$separator) {
        return $array;
    }

    $result = [];

    foreach ($array as $item) {
        foreach (explode($separator, $item) as $val) {
            $result[] = trim($val);
        }
    }

    return array_unique($result);
}

function get_option_property($obj, $property)
{
    $obj = json_decode($obj);

    if (!is_object($obj)) {
        return null;
    }

    if (property_exists($obj, $property)) {
        return $obj->$property;
    }

    return null;
}

function tverta($date)
{
    return verta($date)->timezone(config('app.timezone'));
}

function application_installed()
{
    return file_exists(storage_path('installed'));
}

function change_env($key, $value)
{
    // Read .env-file
    $env = file_get_contents(base_path() . '/.env');

    // Split string on every " " and write into array
    $env = preg_split('/\s+/', $env);;

    // Loop through .env-data
    foreach ($env as $env_key => $env_value) {

        // Turn the value into an array and stop after the first split
        // So it's not possible to split e.g. the App-Key by accident
        $entry = explode("=", $env_value, 2);

        // Check, if new key fits the actual .env-key
        if ($entry[0] == $key) {
            // If yes, overwrite it with the new one
            $env[$env_key] = $key . "=" . $value;
        } else {
            // If not, keep the old one
            $env[$env_key] = $env_value;
        }
    }

    // Turn the array back to an String
    $env = implode("\n", $env);

    // And overwrite the .env with the new data
    file_put_contents(base_path() . '/.env', $env);
}

function get_current_theme()
{
    $current_theme = option('current_theme', 'DefaultTheme');

    if (file_exists(base_path() . '/themes/' . $current_theme)) {
        $theme = [];
        $theme['name'] = $current_theme;
        $theme['service_provider'] = "Themes\\$current_theme\src\ThemeServiceProvider";
        return $theme;
    }

    return null;
}

function customConfig($path)
{
    if (file_exists($path)) {
        $config = include $path;
        return $config;
    }
}

function str_random($length)
{
    return Str::random($length);
}

function get_svg_contents($path, $default = '')
{
    if (file_exists(public_path($path))) {

        $file_parts = pathinfo($path);

        if ($file_parts['extension'] == 'svg') {
            return file_get_contents(public_path($path));
        }
    }

    return $default;
}
