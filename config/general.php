<?php

return [

    'api_url' => 'http://ideal-api.test/api/v1',

    'admin_route_prefix' => '',

    'permissions' => [

        'users' => [
            'title'  => 'مدیریت کاربران',
            'values' => [
                'index'    => 'لیست کاربران',
                'view'     => 'مشاهده کاربر',
                'create'   => 'ایجاد کاربر',
                'update'   => 'ویرایش کاربر',
                'delete'   => 'حذف کاربر',
            ]
        ],

        'posts' => [
            'title'  => 'مدیریت نوشته ها',
            'values' => [
                'index'      => 'لیست نوشته ها',
                'create'     => 'ایجاد نوشته',
                'update'     => 'ویرایش نوشته',
                'delete'     => 'حذف نوشته',
                'category'   => 'مدیریت دسته بندی ها',
            ]
        ],

        'products' => [
            'title'  => 'مدیریت محصولات',
            'values' => [
                'index'         => 'لیست محصولات',
                'create'        => 'ایجاد محصول',
                'update'        => 'ویرایش محصول',
                'delete'        => 'حذف محصول',
                'category'      => 'مدیریت دسته بندی ها',
                'spectypes'     => 'مدیریت نوع مشخصات',
                'stock-notify'  => 'مدیریت لیست اطلاع از موجودی',
                'brands'        => 'مدیریت برندها',
                'prices'        => 'قیمت ها',
            ]
        ],

        'attributes' => [
            'title'  => 'مدیریت ویژگی ها',
            'values' => [
                'groups.index'    => 'لیست گروه ویژگی ها',
                'groups.show'     => 'مشاهده گروه ویژگی',
                'groups.create'   => 'ایجاد گروه ویژگی',
                'groups.update'   => 'ویرایش گروه ویژگی',
                'groups.delete'   => 'حذف گروه ویژگی',

                'index'           => 'لیست ویژگی ها',
                'create'          => 'ایجاد ویژگی',
                'update'          => 'ویرایش ویژگی',
                'delete'          => 'حذف ویژگی',
            ]
        ],

        'filters' => [
            'title'  => 'مدیریت فیلترها',
            'values' => [
                'index'    => 'لیست فیلترها',
                'create'   => 'ایجاد فیلتر',
                'update'   => 'ویرایش فیلتر',
                'delete'   => 'حذف فیلتر',
            ]
        ],

        'orders' => [
            'title'  => 'مدیریت سفارشات',
            'values' => [
                'index'             => 'لیست سفارشات',
                'view'              => 'مشاهده سفارش',
                'update'            => 'ویرایش سفارش',
                'delete'            => 'حذف سفارش',
                'shipping-cost'     => 'مدیریت هزینه های ارسال',
            ]
        ],

        'sliders' => [
            'title'  => 'مدیریت اسلایدرها',
            'values' => [
                'index'             => 'لیست اسلایدرها',
                'create'            => 'ایجاد اسلایدر',
                'update'            => 'ویرایش اسلایدر',
                'delete'            => 'حذف اسلایدر',
            ]
        ],

        'banners' => [
            'title'  => 'مدیریت بنرها',
            'values' => [
                'index'             => 'لیست بنرها',
                'create'            => 'ایجاد بنر',
                'update'            => 'ویرایش بنر',
                'delete'            => 'حذف بنر',
            ]
        ],

        'links' => [
            'title'  => 'مدیریت لینک های فوتر',
            'values' => [
                'index'             => 'لیست لینک ها',
                'create'            => 'ایجاد لینک',
                'update'            => 'ویرایش لینک',
                'delete'            => 'حذف لینک',
                'groups'            => 'مدیریت گروه ها'
            ]
        ],

        'backups' => [
            'title'  => 'مدیریت بکاپ ها',
            'values' => [
                'index'             => 'لیست بکاپ ها',
                'create'            => 'ایجاد بکاپ',
                'download'          => 'دانلود بکاپ',
                'delete'            => 'حذف بکاپ',
            ]
        ],

        'pages' => [
            'title'  => 'مدیریت صفحات',
            'values' => [
                'index'             => 'لیست صفحات',
                'create'            => 'ایجاد صفحه',
                'update'            => 'ویرایش صفحه',
                'delete'            => 'حذف صفحه',
            ]
        ],

        'roles' => [
            'title'  => 'مدیریت مقام ها',
            'values' => [
                'index'             => 'لیست مقام ها',
                'create'            => 'ایجاد مقام',
                'update'            => 'ویرایش مقام',
                'delete'            => 'حذف مقام',
            ]
        ],

        'statistics' => [
            'title'  => 'گزارشات',
            'values' => [
                'views'               => 'بازدیدها',
                'viewers'             => 'بازدیدکنندگان',
            ]
        ],

        'themes' => [
            'title'  => 'مدیریت قالب ها',
            'values' => [
                'index'             => 'لیست قالب ها',
                'create'            => 'افزودن قالب',
                'update'            => 'تغییر قالب',
                'delete'            => 'حذف قالب',
            ]
        ],

        'file-manager'    => 'مدیریت فایل ها',

        'menus' => [
            'title'  => 'مدیریت منو ها',
            'values' => [
                'index'             => 'لیست منو ها',
                'create'            => 'ایجاد منو',
                'update'            => 'ویرایش منو',
                'delete'            => 'حذف منو',
            ]
        ],

        'transactions' => [
            'title'  => 'مدیریت تراکنش ها',
            'values' => [
                'index'             => 'لیست تراکنش ها',
                'view'              => 'مشاهده تراکنش',
                'delete'            => 'حذف تراکنش',
            ]
        ],

        'contacts' => [
            'title'  => 'مدیریت تماس با ما',
            'values' => [
                'index'             => 'لیست تماس با ما',
                'view'              => 'مشاهده تماس با ما',
                'delete'            => 'حذف تماس با ما',
            ]
        ],

        'comments' => [
            'title'  => 'مدیریت نظرات',
            'values' => [
                'index'             => 'لیست نظرات',
                'view'              => 'مشاهده نظر',
                'update'             => 'ویرایش نظر',
                'delete'            => 'حذف نظر',
            ]
        ],

        'settings' => [
            'title'  => 'تنظیمات',
            'values' => [
                'information'        => 'اطلاعات سایت',
                'socials'            => 'شبکه های اجتماعی',
                'gateway'            => 'درگاه های پرداخت',
                'others'             => 'تنظیمات دیگر',
            ]
        ],

    ],

    'static_menus' => [
        'posts' => [
            'title' => 'وبلاگ'
        ],
        'products' => [
            'title' => 'محصولات',
        ]
    ]
];
