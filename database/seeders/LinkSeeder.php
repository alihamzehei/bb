<?php

namespace Database\Seeders;

use App\Models\LinkGroup;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('front.linkGroups')) {
            foreach (config('front.linkGroups') as $linkGroup) {
                LinkGroup::create([
                    'title' => $linkGroup['name'],
                ]);
            }
        }

        $groups = LinkGroup::all();

        foreach ($groups as $group) {
            for ($i = 0; $i < 4; $i++) {
                $group->links()->create([
                    'title'    => 'تست',
                    'link'     => '#',
                    'ordering' => $i
                ]);
            }
        }
    }
}
