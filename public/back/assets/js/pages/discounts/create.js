$('#discount-products-include').on('change', function() {
    switch ($(this).val()) {
        case "all":
            {
                $('#categories-include').hide();
                $('#products-include').hide();
                break;
            }
        case "category":
            {
                $('#categories-include').show();
                $('#products-include').hide();
                break;
            }
        case "product":
            {
                $('#categories-include').hide();
                $('#products-include').show();
                break;
            }
    }
});

$('#discount-products-exclude').on('change', function() {
    switch ($(this).val()) {
        case "none":
            {
                $('#categories-exclude').hide();
                $('#products-exclude').hide();
                break;
            }
        case "category":
            {
                $('#categories-exclude').show();
                $('#products-exclude').hide();
                break;
            }
        case "product":
            {
                $('#categories-exclude').hide();
                $('#products-exclude').show();
                break;
            }
    }
});


$('#users-include, #products-include-select, #categories-include-select, #categories-exclude-select, #products-exclude-select').select2({
    rtl: true,
    width: '100%',
});