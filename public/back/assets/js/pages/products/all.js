CKEDITOR.config.height = 400;
CKEDITOR.replace('description');

$('.tags').tagsInput({
    'defaultText': 'افزودن',
    'width': '100%',
    'autocomplete_url': BASE_URL + '/get-tags',
});

$('.product-category').select2({
    rtl: true,
    width: '100%',
});

// validate form with jquery validation plugin
jQuery('#physical-product-create-form, #physical-product-edit-form').validate({

    rules: {
        'title': {
            required: true,
        },
        'price': {
            required: true,
            digits: true,
            min: 100,
            max: 50000000

        },
        'discount': {
            digits: true,
            max: 100

        },
        'weight': {
            required: true,
            digits: true

        },
        'stock': {
            required: true,
            digits: true

        },

    },
});

// validate form with jquery validation plugin
jQuery('#download-product-create-form').validate({
    rules: {
        'title': {
            required: true,
        },
        'file': {
            required: true,
        },
        'price': {
            digits: true,
            min: 100,
            max: 50000000
        },
        'discount': {
            digits: true,
            max: 100
        },
    },
});

jQuery('#download-product-edit-form').validate({
    rules: {
        'title': {
            required: true,
        },
        'price': {
            digits: true,
            min: 100,
            max: 50000000
        },
        'discount': {
            digits: true,
            max: 100
        },
    },
});

//------------ specification group js codes

var groupsCount = groupCount;

$('#add-product-specification-group').click(function() {
    var template = $('#specification-group').clone();

    var group = $('#specifications-area').append(template.html());

    var count = ++groupCount;
    groupsCount++;

    var input = group.find('input[name="specification_group"]');

    input.attr('name', 'specification_group[' + (count) + '][name]');
    input.data('group_name', count);

    groupSortable();

    setTimeout(() => {
        group.find('.specification-group').removeClass('.animated fadeIn');
    }, 700);
});

function groupSortable() {
    $('#specifications-area').sortable({
        opacity: .75,
        start: function(e, ui) {
            ui.placeholder.css({
                'height': ui.item.outerHeight(),
                'margin-bottom': ui.item.css('margin-bottom'),
            });
        },
        helper: function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        },

    });
}

groupSortable();

$(document).on('click', '.remove-group', function() {
    var group = $(this).closest('.specification-group');

    group.addClass('animated fadeOut');

    setTimeout(() => {
        group.remove();
    }, 500);

    groupsCount--;
});

//------------ specifications js codes

$(document).on('click', '.add-specifaction', function() {
    var template = $('#specification-single').clone();

    var specification = $(this).closest('.specification-group').find('.all-specifications').append(template.html());

    var count = ++specificationCount;
    var group_name = $(specification).closest('.specification-group').find('.group-input').data('group_name');

    specification.find('input[name="special_specification"]').attr('name', 'specification_group[' + (group_name) + '][specifications][' + count + '][special]');
    specification.find('input[name="specification_name"]').attr('name', 'specification_group[' + (group_name) + '][specifications][' + count + '][name]');
    specification.find('textarea[name="specification_value"]').attr('name', 'specification_group[' + (group_name) + '][specifications][' + count + '][value]');

    specificationSortable();

    setTimeout(() => {
        specification.find('.single-specificition').removeClass('.animated fadeIn');
    }, 700);

});

$(document).on('click', '.remove-specification', function() {
    var specification = $(this).closest('.single-specificition');

    specification.addClass('animated fadeOut');

    setTimeout(() => {
        specification.remove();
    }, 500);
});

function specificationSortable() {
    $('.all-specifications').sortable({
        opacity: .75,
        start: function(e, ui) {
            ui.placeholder.css({
                'height': ui.item.outerHeight(),
                'margin-bottom': ui.item.css('margin-bottom'),
            });
        },
        helper: function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        },

    });
}

specificationSortable();


//------------ spectype js codes

$("#specifications_type").autocomplete({
    source: availableTypes
});

$('#specifications_type').change(function() {
    var value = $(this).val();

    if ((availableTypes.includes(value) && !specifications_type_first_change)) {
        addSpecTypeData();
    } else if (availableTypes.includes(value) && groupsCount != 0) {
        $('#specifications-modal').modal('show');
    } else if (availableTypes.includes(value) && groupsCount == 0) {
        addSpecTypeData();
    }

    specifications_type_first_change = true;

    $('#spec-div').show();
});

$('#add-spec-type-data').click(addSpecTypeData);

$('#specifications_type').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

function addSpecTypeData() {
    $.ajax({
        url: BASE_URL + '/spectypes/spec-type-data',
        type: 'GET',
        data: {
            name: $('#specifications_type').val()
        },
        success: function(data) {
            groupCount = data.groupCount;
            specificationCount = data.specificationCount;
            groupsCount = data.groupCount;

            $('#specifications-area').html(data.view);
            specificationSortable();
            groupSortable();

        },
        beforeSend: function(xhr) {
            block('#specifications-card');
        },
        complete: function() {
            unblock('#specifications-card');
        },
    });
}

//------------ prices js codes

$('.price-type-collapse').click(function(e) {
    $(this).find('input[name="price_type"]').prop('checked', true);
});

$('#add-product-prices').click(function() {
    addProductPrice();

});

$(document).on('click', '.remove-product-price', function() {
    var price = $(this).closest('.single-price');

    price.addClass('animated fadeOut');

    setTimeout(() => {
        price.remove();
    }, 500);
});

if (priceCount == 0) {
    addProductPrice();
}

function addProductPrice() {
    var template = $('#prices-template').clone();

    var price = $('#product-prices-div').append(template.html());

    var count = ++priceCount;

    price.find('select[name="attribute"]').attr('name', 'prices[' + count + '][attributes][]');

    price.find('input[name="price"]').attr('name', 'prices[' + count + '][price]');
    price.find('input[name="discount"]').attr('name', 'prices[' + count + '][discount]');
    price.find('input[name="cart_max"]').attr('name', 'prices[' + count + '][cart_max]');
    price.find('input[name="cart_min"]').attr('name', 'prices[' + count + '][cart_min]');
    price.find('input[name="stock"]').attr('name', 'prices[' + count + '][stock]');

    setTimeout(() => {
        price.find('.single-price').removeClass('.animated fadeIn');
    }, 700);
}

// generate slug

$('#generate-product-slug').click(function(e) {
    e.preventDefault();

    var title = $('input[name="meta_title"]').val();

    $.ajax({
        url: BASE_URL + '/product/slug',
        type: 'POST',
        data: {
            title: title
        },
        success: function(data) {
            $('#slug').val(data.slug);
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            $('#slug-spinner').show();
        },
        complete: function() {
            $('#slug-spinner').hide();
        }
    });
});

// dropzone sortable

$('.dropzone-area').sortable({
    items: '.dz-preview',
    opacity: .75,
    start: function(e, ui) {
        ui.placeholder.css({
            'height': ui.item.outerHeight(),
            'margin-bottom': ui.item.css('margin-bottom'),
        });
    },
    helper: function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    },

});

//------------ spectype js codes

$("#brand").autocomplete({
    source: BASE_URL + '/brands/ajax/get',
    delay: 1000
});

//------------ publish time picker js codes

$('#publish_date_picker').on('keydown', function(e) {
    e.preventDefault();
    $(this).val('');
    $('#publish_date').val('');
});