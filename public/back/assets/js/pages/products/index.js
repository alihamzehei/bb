$(document).on('click', '.btn-delete', function() {
    $('#product-delete-form').attr('action', BASE_URL + '/products/' + $(this).data('product'));
    $('#product-delete-form').data('id', $(this).data('id'));
});

$('#product-delete-form').submit(function(e) {
    e.preventDefault();

    $('#delete-modal').modal('hide');

    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {
            //get current url
            var url = window.location.href;

            //remove product tr
            $('#product-' + $(this).data('id') + '-tr').remove();

            toastr.success('محصول با موفقیت حذف شد.');

            //refresh products list
            $(".app-content").load(url + " .app-content > *");
        },
        beforeSend: function(xhr) {
            block('#main-card');
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function() {
            unblock('#main-card');
        },
        cache: false,
        contentType: false,
        processData: false
    });


});