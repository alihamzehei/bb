$(document).ready(function() {
    /*=========+===================
      Gateway Tab Js Codes
    ===============================*/

    $('#mellat').change(function() {
        if ($(this).prop('checked')) {
            $('.mellat').prop('disabled', false);
        } else {
            $('.mellat').prop('disabled', true);
        }
    });

    $('#payir').change(function() {
        if ($(this).prop('checked')) {
            $('.payir').prop('disabled', false);
        } else {
            $('.payir').prop('disabled', true);
        }
    });

    $('#mellat').trigger('change');
    $('#payir').trigger('change');

    // validate form with jquery validation plugin
    jQuery('#gateway-form').validate();

    $('#gateway-form').submit(function(e) {
        e.preventDefault();

        if ($(this).valid()) {
            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {
                    Swal.fire({
                        type: 'success',
                        title: 'تغییرات با موفقیت ذخیره شد',
                        confirmButtonClass: 'btn btn-primary',
                        confirmButtonText: 'باشه',
                        buttonsStyling: false,
                    })
                },
                beforeSend: function(xhr) {
                    block('#main-card');
                    xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                },
                complete: function() {
                    unblock('#main-card');
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }

    });
});