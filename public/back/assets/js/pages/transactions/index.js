$(document).on('click', '.btn-delete', function() {
    $('#transaction-delete-form').attr('action', BASE_URL + '/transactions/' + $(this).data('transaction'));
    $('#transaction-delete-form').data('id', $(this).data('transaction'));
});

$('#transaction-delete-form').submit(function(e) {
    e.preventDefault();

    $('#delete-modal').modal('hide');

    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function(data) {
            //get current url
            var url = window.location.href;

            //remove transaction tr
            $('#transaction-' + $('#transaction-delete-form').data('id') + '-tr').remove();

            toastr.success('تراکنش با موفقیت حذف شد.');

            //refresh transactions list
            $(".app-content").load(url + " .app-content > *");
        },
        beforeSend: function(xhr) {
            block('#main-card');
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        complete: function() {
            unblock('#main-card');
        },
        cache: false,
        contentType: false,
        processData: false
    });


});


$(document).on('click', '.show-transaction', function() {
    $.ajax({
        url: BASE_URL + '/transactions/' + $(this).data('transaction'),
        type: 'GET',
        success: function(data) {
            $('#transaction-detail').empty();
            $('#transaction-detail').append(data);
            $('#show-modal').modal('show');

        },
        beforeSend: function(xhr) {
            block('#main-card');
        },
        complete: function() {
            unblock('#main-card');
        }
    });

});