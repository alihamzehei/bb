<div class="table-responsive">
    <table class="table">
        <tbody>

            <tr>
                <th scope="row">نام</th>
                <td>{{ $contact->name }}</td>

            </tr>

            <tr>
                <th scope="row">موضوع</th>
                <td>{{ $contact->subject }}</td>
            </tr>
            <tr>
                <th scope="row">ایمیل</th>
                <td>{{ $contact->email }}</td>
            </tr>

            <tr>
                <th scope="row">تاریخ ارسال</th>
                <td>{{ tverta($contact->created_at) }}</td>
            </tr>

            <tr>
                <th scope="row">پیام</th>
                <td>{{ $contact->message }}</td>
            </tr>

        </tbody>
    </table>
</div>
