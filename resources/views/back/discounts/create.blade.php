@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/plugins/persian-date/persian-datepicker.min.css') }}">
@endpush

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">مدیریت تخفیفها
                                </li>
                                <li class="breadcrumb-item active">ایجاد تخفیف
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <!-- Description -->
            <section class="card">
                <div class="card-header">
                    <h4 class="card-title">ایجاد تخفیف جدید</h4>
                </div>

                <div id="main-card" class="card-content overflow-hidden">
                    <div class="card-body">
                        <div class="col-12 col-md-12">
                            <form class="form" id="discount-create-form" action="{{ route('admin.discounts.store') }}" method="post">
                                @csrf

                                <div class="nav-vertical">
                                    <ul class="nav nav-tabs nav-left flex-column" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="baseVerticalLeft-tab1" data-toggle="tab" aria-controls="tabVerticalLeft1" href="#tabVerticalLeft1" role="tab" aria-selected="true">اطلاعات</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="baseVerticalLeft-tab2" data-toggle="tab" aria-controls="tabVerticalLeft2" href="#tabVerticalLeft2" role="tab" aria-selected="false">محدودیت ها</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tabVerticalLeft1" role="tabpanel" aria-labelledby="baseVerticalLeft-tab1">
                                            <div class="col-12">
                                                <div class="row">

                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>عنوان</label>
                                                            <input type="text" class="form-control" name="link">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>کد تخفیف</label>
                                                        <div class="input-group form-group">
                                                            <input type="text" class="form-control">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">ایجاد خودکار</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label>تاریخ شروع</label>
                                                        <div class="input-group form-group">
                                                            <input type="text" class="form-control">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>تاریخ پایان</label>
                                                        <div class="input-group form-group">
                                                            <input type="text" class="form-control">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>نوع تخفیف</label>
                                                            <select class="form-control" name="group">
                                                                <option value="">درصد</option>
                                                                <option value="">مبلغ</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>مقدار تخفیف</label>
                                                            <input type="text" class="form-control" name="link" placeholder="تومان یا درصد">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>کمترین مبلغ سفارش</label>
                                                            <input type="text" class="form-control" name="link">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>کمترین تعداد محصول در سبد</label>
                                                            <input type="text" class="form-control" name="link">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-12">
                                                        <div class="form-group">
                                                            <label>توضیحات</label>
                                                            <textarea class="form-control" rows="3" name="link"></textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-12 col-md-4">
                                                        <fieldset class="checkbox">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input type="checkbox" name="only_first_buy">
                                                                <span class="vs-checkbox">
                                                                    <span class="vs-checkbox--check">
                                                                        <i class="vs-icon feather icon-check"></i>
                                                                    </span>
                                                                </span>
                                                                <span>فقط برای اولین خرید</span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-md-4">
                                                        <fieldset class="checkbox">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input type="checkbox" name="only_no_discount" checked>
                                                                <span class="vs-checkbox">
                                                                    <span class="vs-checkbox--check">
                                                                        <i class="vs-icon feather icon-check"></i>
                                                                    </span>
                                                                </span>
                                                                <span>فقط برای محصولات بدون تخفیف</span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-md-3">
                                                        <fieldset class="checkbox">
                                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input type="checkbox" name="published" checked>
                                                                <span class="vs-checkbox">
                                                                    <span class="vs-checkbox--check">
                                                                        <i class="vs-icon feather icon-check"></i>
                                                                    </span>
                                                                </span>
                                                                <span>انتشار تخفیف؟</span>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tabVerticalLeft2" role="tabpanel" aria-labelledby="baseVerticalLeft-tab2">
                                            <div class="col-12">
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>انتخاب مشتریان</label>
                                                            <select id="users-include" class="form-control" name="group" multiple>
                                                                <option value="">کاربر 1</option>
                                                                <option value="">کاربر 2</option>
                                                                <option value="">کاربر 3</option>
                                                                <option value="">کاربر 4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>تعداد مجاز برای استفاده</label>
                                                            <input type="text" class="form-control" name="link">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>تعداد مجاز برای هر کاربر</label>
                                                            <input type="text" class="form-control" name="link">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>محصولاتی که شامل تخیف میشوند</label>
                                                            <select id="discount-products-include" class="form-control" name="group">
                                                                <option value="all">اعمال روی همه محصولات</option>
                                                                <option value="category">انتخاب دسته بندی</option>
                                                                <option value="product">انتخاب محصول</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div id="categories-include" class="form-group" style="display: none;">
                                                            <label>انتخاب دسته بندی</label>
                                                            <select id="categories-include-select" class="form-control" name="group" multiple>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                            </select>
                                                        </div>
                                                        <div id="products-include" class="form-group" style="display: none;">
                                                            <label>انتخاب محصولات</label>
                                                            <select id="products-include-select" class="form-control" name="group" multiple>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>محصولاتی که شامل تخیف نمی شوند</label>
                                                            <select id="discount-products-exclude" class="form-control" name="group">
                                                                <option value="none">هیچ کدام</option>
                                                                <option value="category">انتخاب دسته بندی</option>
                                                                <option value="product">انتخاب محصول</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div id="categories-exclude" class="form-group" style="display: none;">
                                                            <label>انتخاب دسته بندی</label>
                                                            <select id="categories-exclude-select" class="form-control" name="group" multiple>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                            </select>
                                                        </div>
                                                        <div id="products-exclude" class="form-group" style="display: none;">
                                                            <label>انتخاب محصولات</label>
                                                            <select id="products-exclude-select" class="form-control" name="group" multiple>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                                <option value="">تست</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-primary mb-1 waves-effect waves-light">ایجاد تخفیف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Description -->

        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>

    <script src="{{ v_asset('back/app-assets/plugins/persian-date/persian-date.min.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/persian-date/persian-datepicker.min.js') }}"></script>

    <script src="{{ v_asset('back/assets/js/pages/discounts/create.js') }}"></script>
@endpush
