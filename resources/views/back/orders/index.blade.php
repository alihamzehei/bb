@extends('back.layouts.master')

@push('styles')

    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/plugins/datatable/datatable.css', 2) }}">
@endpush

@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item">مدیریت سفارشات
                                    </li>
                                    <li class="breadcrumb-item active">لیست سفارشات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div> 

            </div>

            <div class="content-body">

                <section class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست سفارشات</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ v_asset('back/app-assets/plugins/datatable/scripts.bundle.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/datatable/core.datatable.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/datatable/datatable.checkbox.js') }}"></script>

    <!-- END: Page Vendor JS-->

    <script src="{{ v_asset('back/assets/js/pages/orders/index.js', 2) }}"></script>
@endpush