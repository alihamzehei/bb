<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ url('/') }}" target="_blank">
                    <h2 class="brand-text mb-0">ایده آل</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="{{ active_class('admin.dashboard') }} nav-item"><a href="{{ route('admin.dashboard') }}">
                    <i class="feather icon-home"></i>
                    <span class="menu-title">داشبورد</span>
                </a>
            </li>

            @can('users')
                <li class="nav-item has-sub {{ open_class(['admin.users.show']) }}"><a href="#"><i class="feather icon-users"></i><span class="menu-title" >مدیریت کاربران</span></a>
                    <ul class="menu-content">
                        @can('users.index')
                            <li class="{{ active_class('admin.users.index') }}">
                                <a href="{{ route('admin.users.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست کاربران</span></a>
                            </li>
                        @endcan

                        @can('users.create')
                            <li class="{{ active_class('admin.users.create') }}">
                                <a href="{{ route('admin.users.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد کاربر</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('posts')
                <li class="nav-item has-sub {{ open_class(['admin.posts.edit']) }}"><a href="#"><i class="feather icon-file-text"></i><span class="menu-title" >مدیریت وبلاگ</span></a>
                    <ul class="menu-content">
                        @can('posts.index')
                            <li class="{{ active_class('admin.posts.index') }}">
                                <a href="{{ route('admin.posts.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست نوشته ها</span></a>
                            </li>
                        @endcan

                        @can('posts.create')
                            <li class="{{ active_class('admin.posts.create') }}">
                                <a href="{{ route('admin.posts.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد نوشته</span></a>
                            </li>
                        @endcan

                        @can('posts.category')
                            <li class="{{ active_class('admin.posts.categories.index') }}">
                                <a href="{{ route('admin.posts.categories.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسته بندی ها</span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('products')
                <li class="nav-item has-sub {{ open_class(['admin.products.edit']) }}"><a href="#"><i class="feather icon-shopping-cart"></i><span class="menu-title" >مدیریت محصولات</span></a>
                    <ul class="menu-content">
                        @can('products.index')
                            <li class="{{ active_class('admin.products.index') }}">
                                <a href="{{ route('admin.products.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست محصولات</span></a>
                            </li>
                        @endcan

                        @can('products.create')
                            <li class="{{ active_class('admin.products.create') }}">
                                <a href="{{ route('admin.products.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد محصول</span></a>
                            </li>
                        @endcan

                        @can('products.category')
                            <li class="{{ active_class('admin.products.categories.index') }}">
                                <a href="{{ route('admin.products.categories.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسته بندی ها</span></a>
                            </li>
                        @endcan

                        @can('products.spectypes')
                            <li class="{{ active_class('admin.spectypes.index') }}">
                                <a href="{{ route('admin.spectypes.index') }}"><i class="feather icon-circle"></i><span class="menu-item">نوع مشخصات</span></a>
                            </li>
                        @endcan

                        @can('products.stock-notify')
                            <li class="{{ active_class('admin.stock-notifies.index') }}">
                                <a href="{{ route('admin.stock-notifies.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست اطلاع از موجودی</span></a>
                            </li>
                        @endcan

                        @can('products.prices')
                            <li class="{{ active_class('admin.product.prices.index') }}">
                                <a href="{{ route('admin.product.prices.index') }}"><i class="feather icon-circle"></i><span class="menu-item">قیمت ها</span></a>
                            </li>
                        @endcan

                        @can('products.brands')
                            <li class="{{ open_class(['admin.brands.edit']) }}">
                                <a href="#"><i class="feather icon-circle"></i><span class="menu-item">مدیریت برندها</span></a>
                                <ul class="menu-content">
                                    <li class="{{ active_class('admin.brands.index') }}">
                                        <a class="{{ active_class('admin.brands.index') }}" href="{{ route('admin.brands.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست برندها</span></a>
                                    </li>
                                    <li class="{{ active_class('admin.brands.create') }}">
                                        <a class="{{ active_class('admin.brands.create') }}" href="{{ route('admin.brands.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد برند</span></a>
                                    </li>
                                </ul>
                            </li>
                        @endcan

                        @can('attributes')
                            <li class="{{ open_class(['admin.attributeGroups.edit']) }}">
                                <a href="#"><i class="feather icon-circle"></i><span class="menu-item">مدیریت ویژگی ها</span></a>
                                <ul class="menu-content">
                                    @can('attributes.groups.index')
                                        <li class="{{ active_class('admin.attributeGroups.index') }}">
                                            <a class="{{ active_class('admin.attributeGroups.index') }}" href="{{ route('admin.attributeGroups.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست گروه ویژگی ها</span></a>
                                        </li>
                                    @endcan

                                    @can('attributes.groups.create')
                                        <li class="{{ active_class('admin.attributeGroups.create') }}">
                                            <a class="{{ active_class('admin.attributeGroups.create') }}" href="{{ route('admin.attributeGroups.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد گروه ویژگی</span></a>
                                        </li>
                                    @endcan

                                    @can('attributes.create')
                                        <li class="{{ active_class('admin.attributes.create') }}">
                                            <a class="{{ active_class('admin.attributes.create') }}" href="{{ route('admin.attributes.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد ویژگی</span></a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan

                        @can('filters')
                            <li class="{{ open_class(['admin.filters.edit']) }}">
                                <a href="#"><i class="feather icon-circle"></i><span class="menu-item">مدیریت فیلترها</span></a>
                                <ul class="menu-content">
                                    @can('filters.index')
                                        <li class="{{ active_class('admin.filters.index') }}">
                                            <a class="{{ active_class('admin.filters.index') }}" href="{{ route('admin.filters.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست فیلتر ها</span></a>
                                        </li>
                                    @endcan

                                    @can('filters.create')
                                        <li class="{{ active_class('admin.filters.create') }}">
                                            <a class="{{ active_class('admin.filters.create') }}" href="{{ route('admin.filters.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد فیلتر</span></a>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan

                        <li class="{{ open_class(['admin.discounts.edit']) }}">
                            <a href="#"><i class="feather icon-circle"></i><span class="menu-item">مدیریت تخفیف ها</span></a>
                            <ul class="menu-content">
                                <li class="{{ active_class('admin.discounts.index') }}">
                                    <a class="{{ active_class('admin.discounts.index') }}" href="{{ route('admin.discounts.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست تخفیف ها</span></a>
                                </li>

                                <li class="{{ active_class('admin.discounts.create') }}">
                                    <a class="{{ active_class('admin.discounts.create') }}" href="{{ route('admin.discounts.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد تخفیف</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            @endcan

            @can('orders')
                <li class="nav-item has-sub {{ open_class(['admin.orders.show']) }}"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" >مدیریت سفارشات</span></a>
                    <ul class="menu-content">
                        @can('orders.index')
                            <li class="{{ active_class('admin.orders.index') }}">
                                <a href="{{ route('admin.orders.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست سفارشات</span></a>
                            </li>
                        @endcan

                        @can('orders.shipping-cost')
                            <li class="{{ active_class('admin.shipping-costs.index') }}">
                                <a href="{{ route('admin.shipping-costs.index') }}"><i class="feather icon-circle"></i><span class="menu-item"> هزینه های ارسال</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('sliders')
                <li class="nav-item has-sub {{ open_class(['admin.sliders.show', 'admin.sliders.edit']) }}"><a href="#"><i class="feather icon-sliders"></i><span class="menu-title" >مدیریت اسلایدرها</span></a>
                    <ul class="menu-content">
                        @can('sliders.index')
                            <li class="{{ active_class('admin.sliders.index') }}">
                                <a href="{{ route('admin.sliders.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست اسلایدرها</span></a>
                            </li>
                        @endcan

                        @can('sliders.create')
                            <li class="{{ active_class('admin.sliders.create') }}">
                                <a href="{{ route('admin.sliders.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد اسلایدر</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('banners')
                <li class="nav-item has-sub {{ open_class(['admin.banners.show', 'admin.banners.edit']) }}"><a href="#"><i class="feather icon-image"></i><span class="menu-title" >مدیریت بنرها</span></a>
                    <ul class="menu-content">
                        @can('banners.index')
                            <li class="{{ active_class('admin.banners.index') }}">
                                <a href="{{ route('admin.banners.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست بنرها</span></a>
                            </li>
                        @endcan

                        @can('banners.create')
                            <li class="{{ active_class('admin.banners.create') }}">
                                <a href="{{ route('admin.banners.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد بنر</span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('links')
                <li class="nav-item has-sub {{ open_class(['admin.links.show', 'admin.links.edit']) }}"><a href="#"><i class="feather icon-link"></i><span class="menu-title" >مدیریت لینک های فوتر</span></a>
                    <ul class="menu-content">
                        @can('links.index')
                            <li class="{{ active_class('admin.links.index') }}">
                                <a href="{{ route('admin.links.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست لینک ها</span></a>
                            </li>
                        @endcan

                        @can('links.create')
                            <li class="{{ active_class('admin.links.create') }}">
                                <a href="{{ route('admin.links.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد لینک </span></a>
                            </li>
                        @endcan

                        @can('links.groups')
                            <li class="{{ active_class('admin.links.groups.index') }}">
                                <a href="{{ route('admin.links.groups.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست گروه ها </span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('pages')
                <li class="nav-item has-sub {{ open_class(['admin.pages.edit']) }}"><a href="#"><i class="feather icon-file"></i><span class="menu-title" >مدیریت صفحات</span></a>
                    <ul class="menu-content">
                        @can('pages.index')
                            <li class="{{ active_class('admin.pages.index') }}">
                                <a href="{{ route('admin.pages.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست صفحات</span></a>
                            </li>
                        @endcan

                        @can('pages.create')
                            <li class="{{ active_class('admin.pages.create') }}">
                                <a href="{{ route('admin.pages.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد صفحه</span></a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('roles')
                <li class="nav-item has-sub {{ open_class(['admin.roles.edit']) }}"><a href="#"><i class="feather icon-unlock"></i><span class="menu-title" >مدیریت مقام ها</span></a>
                    <ul class="menu-content">
                        @can('roles.index')
                            <li class="{{ active_class('admin.roles.index') }}">
                                <a href="{{ route('admin.roles.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست مقام ها</span></a>
                            </li>
                        @endcan

                        @can('roles.create')
                            <li class="{{ active_class('admin.roles.create') }}">
                                <a href="{{ route('admin.roles.create') }}"><i class="feather icon-circle"></i><span class="menu-item">ایجاد مقام</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('statistics')
                <li class="nav-item has-sub"><a href="#"><i class="feather icon-pie-chart"></i><span class="menu-title" >گزارشات</span></a>
                    <ul class="menu-content">
                        @can('statistics.views')
                            <li class="{{ active_class('admin.statistics.views') }}">
                                <a href="{{ route('admin.statistics.views') }}"><i class="feather icon-circle"></i><span class="menu-item">بازدیدها</span></a>
                            </li>
                        @endcan

                        @can('statistics.viewers')
                            <li class="{{ active_class('admin.statistics.viewers') }}">
                                <a href="{{ route('admin.statistics.viewers') }}"><i class="feather icon-circle"></i><span class="menu-item"> بازدید کنندگان</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('themes')
                <li class="nav-item has-sub"><a href="#"><i class="feather icon-layout"></i><span class="menu-title" >قالب ها</span></a>
                    <ul class="menu-content">
                        @can('themes.index')
                            <li class="{{ active_class('admin.themes.index') }}">
                                <a href="{{ route('admin.themes.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لیست قالب ها</span></a>
                            </li>
                        @endcan

                        @can('themes.create')
                            <li class="{{ active_class('admin.themes.create') }}">
                                <a href="{{ route('admin.themes.create') }}"><i class="feather icon-circle"></i><span class="menu-item">افزودن قالب جدید</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @can('menus')
                <li class="{{ active_class('admin.menus.index') }} nav-item">
                    <a href="{{ route('admin.menus.index') }}">
                        <i class="feather icon-menu"></i>
                        <span class="menu-title">مدیریت منوها</span>
                    </a>
                </li>
            @endcan

            @can('transactions')
                <li class="{{ active_class('admin.transactions.index') }} nav-item">
                    <a href="{{ route('admin.transactions.index') }}">
                        <i class="feather icon-credit-card"></i>
                        <span class="menu-title"> لیست تراکنش ها</span>
                    </a>
                </li>
            @endcan

            @can('contacts')
                <li class="{{ active_class('admin.contacts.index') }} nav-item">
                    <a href="{{ route('admin.contacts.index') }}">
                        <i class="feather icon-message-square"></i>
                        <span class="menu-title">لیست تماس با ما</span>
                    </a>
                </li>
            @endcan

            @can('comments')
                <li class="{{ active_class('admin.comments.index') }} nav-item"><a href="{{ route('admin.comments.index') }}">
                        <i class="feather icon-message-circle"></i>
                        <span class="menu-title">مدیریت نظرات</span>
                    </a>
                </li>
            @endcan

            @can('file-manager')
                <li class="{{ active_class('admin.file-manager') }} nav-item">
                    <a href="{{ route('admin.file-manager') }}">
                        <i class="feather icon-folder"></i>
                        <span class="menu-title">مدیریت فایل ها</span>
                    </a>
                </li>
            @endcan

            @can('backups.index')
                <li class="{{ active_class('admin.backups.index') }} nav-item">
                    <a href="{{ route('admin.backups.index') }}">
                        <i class="feather icon-upload-cloud"></i>
                        <span class="menu-title">لیست بکاپ ها</span>
                    </a>
                </li>
            @endcan

            <li class="{{ active_class('admin.notifications') }} nav-item"><a href="{{ route('admin.notifications') }}">
                    <i class="feather icon-bell"></i>
                    <span class="menu-title">اعلان ها</span>
                    @if($notifications->count())
                        <span class="badge badge badge-primary badge-pill float-right mr-2"> {{ $notifications->count() }}</span>
                    @endif
                </a>
            </li>

            @can('settings')
                <li class="nav-item has-sub">
                    <a href="#">
                        <i class="feather icon-settings"></i>
                        <span class="menu-title" >تنظیمات</span>
                    </a>
                    <ul class="menu-content">
                        @can('settings.information')
                            <li class="{{ active_class('admin.settings.information') }}">
                                <a href="{{ route('admin.settings.information') }}"><i class="feather icon-circle"></i><span class="menu-item">اطلاعات کلی</span></a>
                            </li>
                        @endcan

                        @can('settings.socials')
                            @if (config('front.socials'))
                                <li class="{{ active_class('admin.settings.socials') }}">
                                    <a href="{{ route('admin.settings.socials') }}"><i class="feather icon-circle"></i><span class="menu-item">شبکه های اجتماعی</span></a>
                                </li>
                            @endif
                        @endcan

                        @can('settings.gateway')
                            <li class="{{ active_class('admin.settings.gateways') }}">
                                <a href="{{ route('admin.settings.gateways') }}"><i class="feather icon-circle"></i><span class="menu-item">درگاه های پرداخت</span></a>
                            </li>
                        @endcan

                        @can('settings.others')
                            <li class="{{ active_class('admin.settings.others') }}">
                                <a href="{{ route('admin.settings.others') }}"><i class="feather icon-circle"></i><span class="menu-item">تنظیمات دیگر</span></a>
                            </li>
                        @endcan

                        @if(config('front.additional_fields'))
                            <li class="{{ active_class('admin.settings.themeSettings') }}">
                                <a href="{{ route('admin.settings.themeSettings') }}"><i class="feather icon-circle"></i><span class="menu-item">تنظیمات قالب</span></a>
                            </li>
                        @endcan

                    </ul>
                </li>
            @endcan

            @if(auth()->user()->level == 'creator')
                <li class="nav-item has-sub">
                    <a href="#">
                        <i class="feather icon-alert-octagon"></i>
                        <span class="menu-title" >توسعه دهنده</span>
                        <span class="badge badge-danger badge-pill float-right mr-2">creator</span>
                    </a>
                    <ul class="menu-content">
                        <li class="{{ active_class('admin.permissions.index') }}">
                            <a href="{{ route('admin.permissions.index') }}"><i class="feather icon-circle"></i><span class="menu-item">دسترسی ها</span></a>
                        </li>

                        <li class="{{ active_class('admin.developer.settings') }}">
                            <a href="{{ route('admin.developer.settings') }}"><i class="feather icon-circle"></i><span class="menu-item">تنظیمات توسعه دهنده</span></a>
                        </li>

                        <li class="{{ active_class('admin.logs.index') }}">
                            <a target="_blank" href="{{ route('admin.logs.index') }}"><i class="feather icon-circle"></i><span class="menu-item">لاگ ها</span></a>
                        </li>

                    </ul>
                </li>
            @endif

        </ul>
    </div>
</div>
