@extends('back.layouts.master')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/vendors/css/file-uploaders/dropzone.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/plugins/jquery-ui/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/css-rtl/plugins/file-uploaders/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ v_asset('back/app-assets/plugins/persian-date/persian-datepicker.min.css') }}">
@endpush

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">مدیریت محصولات
                                </li>
                                <li class="breadcrumb-item active">ویرایش محصول
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="main-card" class="content-body">
            <form class="form" id="physical-product-edit-form" action="{{ route('admin.products.update', ['product' => $product]) }}" method="post">
                @csrf
                @method('put')
                <input type="hidden" name="type" value="physical">

                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <h4 class="card-title">اطلاعات محصول</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">

                                    <div class="nav-vertical">
                                        <ul class="nav nav-tabs nav-left flex-column" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="baseVerticalLeft-tab1" data-toggle="tab" aria-controls="tabVerticalLeft1" href="#tabVerticalLeft1" role="tab" aria-selected="true">اطلاعات کلی</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="productMetaTab" data-toggle="tab" aria-controls="tabProductMeta" href="#tabProductMeta" role="tab" aria-selected="false">سئو</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="baseVerticalLeft-tab2" data-toggle="tab" aria-controls="tabVerticalLeft2" href="#tabVerticalLeft2" role="tab" aria-selected="false">قیمت</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="productImageTab" data-toggle="tab" aria-controls="tabProductImage" href="#tabProductImage" role="tab" aria-selected="false">تصاویر</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="specification-tab" data-toggle="tab" aria-controls="tabSpecification" href="#tabSpecification" role="tab" aria-selected="false">مشخصات</a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tabVerticalLeft1" role="tabpanel" aria-labelledby="baseVerticalLeft-tab1">
                                                <div class="col-md-12">
                                                    <div class="form-body">

                                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>عنوان</label>
                                                                    <input type="text" class="form-control" name="title" value="{{ $product->title }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>عنوان انگلیسی</label>
                                                                    <input type="text" class="form-control" name="title_en" value="{{ $product->title_en }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>دسته بندی</label>
                                                                    <select class="form-control product-category" name="category_id">
                                                                        @foreach ($categories as $category)
                                                                            <option {{ ($product->category && $product->category->id == $category->id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->fullTitle }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-12">
                                                                <div class="form-group">
                                                                    <label>برند</label>
                                                                    <input id="brand" type="text" class="form-control" name="brand" value="{{ $product->brand ? $product->brand->name : '' }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-12">
                                                                <div class="form-group">
                                                                    <label>وزن</label>
                                                                    <input type="number" class="form-control" name="weight" value="{{ $product->weight }}">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>توضیحات کوتاه</label>
                                                                    <textarea class="form-control" name="short_description" rows="3">{{ $product->short_description }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="first-name-vertical">توضیحات</label>
                                                                    <textarea id="description" class="form-control" rows="3" name="description">{{ $product->description }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tabVerticalLeft2" role="tabpanel" aria-labelledby="baseVerticalLeft-tab2">

                                                <div class="card collapse-icon accordion-icon-rotate product-prices-tab">

                                                    <div class="card-body">
                                                        <div id="product-prices-div" class="product-prices-div">
                                                            @foreach ($product->prices as $price)
                                                                <div class="row single-price">

                                                                    <div class="col-12">
                                                                        <div class="row">
                                                                            @foreach ($attributeGroups as $attributeGroup)
                                                                                <div class="col-md-3 col-12">
                                                                                    <div class="form-group">
                                                                                        <label>{{ $attributeGroup->name }}</label>
                                                                                        <select class="form-control price-attribute-select" name="prices[{{ $loop->parent->iteration }}][attributes][]">
                                                                                            <option value="">انتخاب کنید</option>
                                                                                            @foreach ($attributeGroup->get_attributes as $attribute)
                                                                                                <option value="{{ $attribute->id }}" {{ $price->get_attributes()->find($attribute->id) ? 'selected' : '' }}>{{ $attribute->name }}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-12">
                                                                        <div class="form-group">
                                                                            <label>قیمت</label>
                                                                            <input type="number" class="form-control" name="prices[{{ $loop->iteration }}][price]" value="{{ $price->price }}" min="100" required>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-12">
                                                                        <div class="form-group">
                                                                            <label>تخفیف</label>
                                                                            <input type="number" class="form-control" name="prices[{{ $loop->iteration }}][discount]" value="{{ $price->discount }}" min="0" max="100" placeholder="%">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-12">
                                                                        <div class="form-group">
                                                                            <label>بیشترین تعداد مجاز در هر سفارش</label>
                                                                            <input type="number" class="form-control" name="prices[{{ $loop->iteration }}][cart_max]" value="{{ $price->cart_max }}" min="1">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-12">
                                                                        <div class="form-group">
                                                                            <label>کمترین تعداد مجاز در هر سفارش</label>
                                                                            <input type="number" class="form-control" name="prices[{{ $loop->iteration }}][cart_min]" value="{{ $price->cart_min }}" min="1">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-12">
                                                                        <div class="form-group">
                                                                            <label>موجودی انبار</label>
                                                                            <input type="number" class="form-control" name="prices[{{ $loop->iteration }}][stock]" value="{{ $price->stock }}" min="0" required>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-12">
                                                                        <button type="button" class="btn btn-flat-danger waves-effect waves-light remove-product-price custom-padding">حذف</i></button>
                                                                    </div>

                                                                    <div class="col-md-12"><hr></div>
                                                                </div>
                                                            @endforeach
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-12 text-center mt-2">
                                                                <button id="add-product-prices" type="button" class="btn btn-outline-primary waves-effect waves-light"><i class="feather icon-plus"></i> افزودن قیمت</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane" id="tabSpecification" role="tabpanel" aria-labelledby="specification-tab">
                                                <div id="specifications-card" class="card">
                                                    <div class="card-header d-flex justify-content-between align-items-end">
                                                        <h4 class="card-title">مشخصات محصول</h4>
                                                        <p class="font-medium-5 mb-0"><i class="feather icon-help-circle text-muted cursor-pointer"></i></p>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="card-body ">
                                                            <div class="row">
                                                                <div class="col-md-7 text-justify">
                                                                    <p>در این بخش میتوانید مشخصات محصول را وارد کنید. دقت  کنید که محصولات بر اساس نوع مشخصات با یکدیگر مقایسه میشوند. به عنوان مثال یک محصول با نوع مشخصات "گوشی موبایل"  فقط با محصولات این نوع مقایسه میشود</p>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>نوع مشخصات</label>
                                                                        <input id="specifications_type" class="form-control" name="spec_type" placeholder="مثلا گوشی موبایل" value="{{ $product->specType ? $product->specType->name : '' }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-body pt-2">
                                                                <div id="specifications-area">
                                                                    @foreach ($product->specificationGroups->unique() as $group)
                                                                        <div class="row mt-2 specification-group">
                                                                            <div class="col-12">
                                                                                <div class="row group-row">
                                                                                    <div class="col-md-1">
                                                                                        <span>نام گروه</span>
                                                                                    </div>
                                                                                    <div class="col-md-10 form-group">
                                                                                        <input type="text" class="form-control group-input" data-group_name="{{ $loop->index }}" name="specification_group[{{ $loop->index }}][name]" placeholder="مثال: مشخصات کلی" value="{{ $group->name }}" required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="all-specifications col-12">
                                                                                @foreach($product->specifications()->where('specification_group_id', $group->id)->get() as $specification)
                                                                                    <div class="single-specificition">
                                                                                        <div class="row">
                                                                                            <div class="col-md-1">
                                                                                                <fieldset>
                                                                                                    <label>
                                                                                                        <input name="specification_group[{{ $loop->parent->index }}][specifications][{{ $loop->index }}][special]" type="checkbox" {{ $specification->pivot->special ? 'checked' : '' }}>
                                                                                                    </label>
                                                                                                </fieldset>
                                                                                            </div>
                                                                                            <div class="col-md-4 form-group">
                                                                                                <p class="spec-label">عنوان</p>
                                                                                                <input type="text" class="form-control spec-label" name="specification_group[{{ $loop->parent->index }}][specifications][{{ $loop->index }}][name]" placeholder="مثال: حافظه داخلی" value="{{ $specification->name }}" required>
                                                                                            </div>

                                                                                            <div class="col-md-6 form-group">
                                                                                                <p class="spec-label">مقدار</p>
                                                                                                <textarea  class="form-control spec-label" rows="1" name="specification_group[{{ $loop->parent->index }}][specifications][{{ $loop->index }}][value]" placeholder="مثال: 32 گیگابایت" required>{{ $specification->pivot->value }}</textarea>
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                <button type="button" class="btn btn-flat-danger waves-effect waves-light remove-specification custom-padding"><i class="feather icon-minus"></i></button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>

                                                                            <div class="col-md-12 text-center">
                                                                                <button type="button" class="btn btn-flat-success waves-effect waves-light add-specifaction">افزودن مشخصات</button>
                                                                                <button type="button" class="btn btn-flat-danger waves-effect waves-light remove-group">حذف گروه</button>

                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-12 text-center mt-4">
                                                                        <button id="add-product-specification-group" type="button" class="btn btn-outline-primary waves-effect waves-light"><i class="feather icon-plus"></i> افزودن گروه مشخصات</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tabProductMeta" role="tabpanel" aria-labelledby="productMetaTab">
                                                <div class="col-md-12">
                                                    <div class="form-body">

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>عنوان سئو</label>
                                                                    <input type="text" class="form-control" name="meta_title" value="{{ $product->meta_title }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>url</label>
                                                                    <input id="slug" type="text" class="form-control" name="slug" value="{{ $product->slug }}">
                                                                    <p>
                                                                        <small >
                                                                            <a id="generate-product-slug" href="#">ایجاد خودکار</a>
                                                                            <span id="slug-spinner" class="spinner-grow spinner-grow-sm text-success" role="status" style="display: none;">
                                                                                <span class="sr-only">Loading...</span>
                                                                            </span>
                                                                        </small>
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>توضیحات سئو</label>
                                                                    <textarea class="form-control" name="meta_description" rows="3">{{ $product->meta_description }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group">
                                                                    <label>کلمات کلیدی</label>
                                                                    <input type="text" name="tags" class="form-control tags" value="{{ $product->getTags }}">
                                                                </fieldset>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane overflow-hidden" id="tabProductImage" role="tabpanel" aria-labelledby="productImageTab">
                                                <div class="col-md-12">
                                                    <div class="form-body">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>تصاویر محصول ( <small>بهترین اندازه <span class="text-danger">{{ option('sizes_product_gallery') }}</span> پیکسل میباشد.</small> )</label>

                                                                <div class="dropzone dropzone-area mb-2" id="product-images">
                                                                    <div class="dz-message">تصاویر را به اینجا بکشید</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <fieldset class="form-group">
                                                                    <label>تصویر شاخص</label>
                                                                    <div class="custom-file">
                                                                        <input id="image" type="file" accept="image/*" name="image" class="custom-file-input">
                                                                        <label class="custom-file-label" for="image">{{ $product->image }}</label>
                                                                        <p><small>بهترین اندازه <span class="text-danger">{{ option('sizes_product_image') }}</span> پیکسل میباشد.</small></p>
                                                                    </div>
                                                                </fieldset>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>متن جایگزین تصویر</label>
                                                                    <input type="text" class="form-control" name="image_alt" value="{{ $product->image_alt }}">
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card">

                    <div class="card-content">
                        <div class="card-body">
                            <div class="form-body">

                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <div class="form-group">
                                            <label>تاریخ انتشار</label>
                                            <input autocomplete="off" type="text" class="form-control" id="publish_date_picker" value="{{ $product->publish_date ? tverta($product->publish_date)->timestamp : '' }}">
                                            <input type="hidden" name="publish_date" id="publish_date" value="{{ $product->publish_date ? tverta($product->publish_date) : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <fieldset class="checkbox">
                                            <div class="vs-checkbox-con vs-checkbox-primary">
                                                <input type="checkbox" name="special" {{ $product->special ? 'checked' : '' }}>
                                                <span class="vs-checkbox">
                                                    <span class="vs-checkbox--check">
                                                        <i class="vs-icon feather icon-check"></i>
                                                    </span>
                                                </span>
                                                <span>محصول ویژه؟</span>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" name="published" id="customRadio1" value="1" {{ $product->published ? 'checked' : '' }}>
                                                        <label class="custom-control-label" for="customRadio1">انتشار</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" name="published" id="customRadio2" value="0" {{ !$product->published ? 'checked' : '' }}>
                                                        <label class="custom-control-label" for="customRadio2">پیش نویس</label>
                                                    </div>
                                                </fieldset>
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">ویرایش محصول</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </form>
            <div id="form-progress" class="progress progress-bar-success progress-xl" style="display: none;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width:0%">0%</div>
            </div>
        </div>
        <div id="form-progress" class="progress progress-bar-success progress-xl" style="display: none;">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width:0%">0%</div>
        </div>

    </div>
</div>

@include('back.products.partials.specification-template')
@include('back.products.partials.prices-template')

@endsection

@push('scripts')
        <script src="{{ v_asset('back/app-assets/plugins/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>

        <script src="{{ v_asset('back/app-assets/plugins/jquery-tagsinput/jquery.tagsinput.min.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/plugins/jquery-ui/jquery-ui.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/plugins/jquery-ui-sortable/jquery-ui.min.js') }}"></script>

        <script src="{{ v_asset('back/app-assets/plugins/persian-date/persian-date.min.js') }}"></script>
        <script src="{{ v_asset('back/app-assets/plugins/persian-date/persian-datepicker.min.js') }}"></script>

        <script>
            /* load saved image gallery */
            var mockImages = [];
            @foreach($product->gallery()->orderBy('ordering')->get() as $image)
                mockImages.push({
                    name: '{{ $image->image}}',
                    galleryImage: true,
                    type: 'image/jpeg',
                    status: 'success',
                    upload: {
                        filename: '{{ $image->image }}',
                    },
                    prevFile: true,
                    accepted: true,
                    image: '{{ $image->image }}',
                });
            @endforeach

            var product = {{ $product->id }};

            var groupCount = {{ $product->specificationGroups->unique()->count() }};
            var specificationCount = {{ $product->specifications->unique()->count() }};

            var availableTypes = [
                @foreach($specTypes as $spec_type)
                    '{{ $spec_type->name }}',
                @endforeach
            ];

            var specifications_type_first_change = true;

            var priceCount = {{ $product->prices->count() }};

        </script>

        <script src="{{ v_asset('back/assets/js/pages/products/all.js', 2) }}"></script>
        <script src="{{ v_asset('back/assets/js/pages/products/edit.js') }}"></script>
@endpush
