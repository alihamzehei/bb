@extends('back.layouts.master')

@section('content')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item">مدیریت محصولات
                                    </li>
                                    <li class="breadcrumb-item active">لیست محصولات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <!-- filter start -->
                @include('back.products.partials.filters', ['filter_action' => route('admin.products.index')])
                <!-- filter end -->

                @if($products->count())
                    <section class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست محصولات</h4>
                        </div>
                        <div class="card-content" id="main-card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <thead>
                                        <tr>
                                            <th class="text-center">تصویر شاخص</th>
                                            <th style="width: 300px;">عنوان</th>
                                            <th class="text-center">وضعیت موجودی</th>
                                            <th class="text-center">وضعیت انتشار</th>
                                            <th class="text-center">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($products as $product)
                                            <tr id="product-{{ $product->id }}-tr">
                                                <td class="text-center">
                                                    <img class="post-thumb"
                                                         src="{{ $product->image ? asset($product->image) : asset('/empty.jpg') }}"
                                                         alt="image">
                                                </td>
                                                <td>{{ $product->title }} <a
                                                            href="{{ Route::has('front.products.show') ? route('front.products.show', ['product' => $product]) : '' }}"
                                                            target="_blank"><i
                                                                class="feather icon-external-link"></i></a></td>

                                                <td class="text-center">
                                                    @if($product->addableToCart())
                                                        <div class="badge badge-pill badge-success badge-md">موجود</div>
                                                    @else
                                                        <div class="badge badge-pill badge-danger badge-md">ناموجود</div>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    @if($product->published)
                                                        <div class="badge badge-pill badge-success badge-md">منتشر شده</div>
                                                    @else
                                                        <div class="badge badge-pill badge-danger badge-md">پیش نویس</div>
                                                    @endif
                                                </td>

                                                <td class="text-center">
                                                    <div class="btn-group dropdown">
                                                        <button type="button" class="btn btn-outline-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            عملیات
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="bottom-start">
                                                            <a class="dropdown-item" href="{{ route('admin.products.edit', ['product' => $product]) }}">ویرایش</a>
                                                            <a class="dropdown-item" href="{{ route('admin.products.create', ['product' => $product]) }}">کپی کردن</a>

                                                            <div class="dropdown-divider"></div>
                                                            <button data-product="{{ $product->slug }}" data-id="{{ $product->id }}" data-toggle="modal" data-target="#delete-modal"class="dropdown-item btn-delete" >حذف</button>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                @else
                    <section class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست محصولات</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-text">
                                    <p>چیزی برای نمایش وجود ندارد!</p>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
                {{ $products->appends(request()->all())->links() }}


            </div>
        </div>
    </div>

    {{-- delete post modal --}}
    <div class="modal fade text-left" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel19">آیا مطمئن هستید؟</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    با حذف محصول دیگر قادر به بازیابی آن نخواهید بود
                </div>
                <div class="modal-footer">
                    <form action="#" id="product-delete-form">
                        @csrf
                        @method('delete')
                        <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">
                            خیر
                        </button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">بله حذف شود</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ v_asset('back/assets/js/pages/products/index.js') }}"></script>
    <script src="{{ v_asset('back/assets/js/pages/products/filters.js') }}"></script>
@endpush
