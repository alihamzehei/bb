@extends('back.layouts.master')

@section('content')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item">مدیریت محصولات
                                    </li>
                                    <li class="breadcrumb-item active">قیمت ها
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <!-- filter start -->
                @include('back.products.partials.filters', ['filter_action' => route('admin.product.prices.index')])
                <!-- filter end -->

                @if($products->count())
                    <form id="prices-update-form" action="{{ route('admin.product.prices.update') }}">
                        @method('put')
                        <section class="card" id="main-card">
                            <div class="card-header">
                                <h4 class="card-title">قیمت ها</h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><button type="button" class="btn btn-outline-primary waves-effect waves-light save-price-changes"><i class="fa fa-save"></i> ذخیره تغییرات</button></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">تصویر شاخص</th>
                                                    <th style="width: 300px;">عنوان</th>
                                                    <th>قیمت و موجودی</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($products as $product)

                                                    @if ($product->price_type == 'multiple-price')
                                                        @foreach ($product->prices as $price)
                                                            <tr id="product-{{ $product->id }}-tr">
                                                                <td class="text-center">
                                                                    <img class="post-thumb"
                                                                        src="{{ $product->image ? asset($product->image) : asset('/empty.jpg') }}"
                                                                        alt="image">
                                                                </td>

                                                                <td>
                                                                    {{ $product->title }}
                                                                    <a href="{{ Route::has('front.products.show') ? route('front.products.show', ['product' => $product]) : '' }}" target="_blank">
                                                                        <i class="feather icon-external-link"></i>
                                                                    </a>
                                                                    <div><small>{{ $price->getAttributesName() }}</small></div>
                                                                </td>
                                                                <td style="width: 400px;">

                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <div class="form-group">
                                                                                <label>قیمت</label>
                                                                                <input name="products[{{ $product->id }}][prices][{{ $price->id }}][price]" type="text" class="form-control" value="{{ $price->price }}">
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <label>موجودی</label>
                                                                                <input name="products[{{ $product->id }}][prices][{{ $price->id }}][stock]" type="text" class="form-control" value="{{ $price->stock }}">
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <p> آخرین تغییر: <span class="price-change-time">{{ tverta($price->updated_at)->formatDifference() }}</span></p>
                                                                        </div>
                                                                    </div>

                                                                </td>


                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr id="product-{{ $product->id }}-tr">
                                                            <td class="text-center">
                                                                <img class="post-thumb"
                                                                    src="{{ $product->image ? asset($product->image) : asset('/empty.jpg') }}"
                                                                    alt="image">
                                                            </td>

                                                            <td>
                                                                {{ $product->title }}
                                                                <a href="{{ Route::has('front.products.show') ? route('front.products.show', ['product' => $product]) : '' }}" target="_blank">
                                                                    <i class="feather icon-external-link"></i>
                                                                </a>
                                                            </td>
                                                            <td style="width: 400px;">

                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <div class="form-group">
                                                                            <label>قیمت</label>
                                                                            <input name="products[{{ $product->id }}][price]" type="text" class="form-control" value="{{ $product->price }}">
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label>موجودی</label>
                                                                            <input name="products[{{ $product->id }}][stock]" type="text" class="form-control" value="{{ $product->stock }}">
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <p> آخرین تغییر: <span class="price-change-time">{{ tverta($product->updated_at)->formatDifference() }}</span></p>
                                                                    </div>
                                                                </div>

                                                            </td>


                                                        </tr>
                                                    @endif

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><button type="button" class="btn btn-outline-primary waves-effect waves-light save-price-changes"><i class="fa fa-save"></i> ذخیره تغییرات</button></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </form>

                @else
                    <section class="card">
                        <div class="card-header">
                            <h4 class="card-title">قیمت ها</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-text">
                                    <p>چیزی برای نمایش وجود ندارد!</p>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
                {{ $products->appends(request()->all())->links() }}


            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ v_asset('back/assets/js/pages/products/filters.js') }}"></script>
    <script src="{{ v_asset('back/assets/js/pages/products/prices.js') }}"></script>
@endpush
