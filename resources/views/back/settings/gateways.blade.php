@extends('back.layouts.master')

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">تنظیمات
                                </li>
                                <li class="breadcrumb-item active">درگاه های پرداخت
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div id="main-card" class="card-content">
                        <div class="card-body">
                            <div class="tab-content">
                                <form id="gateway-form" action="{{ route('admin.settings.gateways') }}" method="POST">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input id="payir" type="checkbox" name="payir" {{ option('gateway_payir_status') == 'on' ? 'checked' : '' }} >
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span class="">درگاه pay.ir</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>api کد</label>
                                            <div class="input-group mb-75">
                                                <input type="text" name="gateway_payir_api" class="form-control ltr payir"
                                                    value="{{ option('gateway_payir_api') }}" required>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input id="mellat" type="checkbox" name="mellat" {{ option('gateway_mellat_status') == 'on' ? 'checked' : '' }}>
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span class="">درگاه بانک ملت</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>نام کاربری</label>
                                            <div class="input-group mb-75">
                                                <input type="text" name="gateway_mellat_username" class="form-control ltr mellat"
                                                    value="{{ option('gateway_mellat_username') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>رمز عبور</label>
                                            <div class="input-group mb-75">
                                                <input type="text" name="gateway_mellat_password" class="form-control ltr mellat"
                                                    value="{{ option('gateway_mellat_password') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>کد پذیرنده</label>
                                            <div class="input-group mb-75">
                                                <input type="text" name="gateway_mellat_terminalId" class="form-control ltr mellat"
                                                    value="{{ option('gateway_mellat_terminalId') }}" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-12">
                                            <div class="alert alert-info mt-1 alert-validation-msg" role="alert">
                                                <i class="feather icon-info ml-1 align-middle"></i>
                                                <span>برای فعال نمودن هر یک از درگاه ها پس از انتخاب درگاه اطلاعات مربوط به آن را پر کنید.</span>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                            <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">ذخیره تغییرات</button>

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->

        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>

    <script src="{{ v_asset('back/assets/js/pages/settings/gateways.js') }}"></script>
@endpush
