@extends('back.layouts.master')

@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item">مدیریت
                                </li>
                                <li class="breadcrumb-item">تنظیمات
                                </li>
                                <li class="breadcrumb-item active">تنظیمات قالب
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- users edit start -->
            <section class="users-edit">
                <div class="card">
                    <div id="main-card" class="card-content">
                        <div class="card-body">

                            <div class="tab-content">

                                <form id="themeSettings-form" action="{{ route('admin.settings.themeSettings') }}" method="POST">
                                    <div class="row">
                                        @foreach (config('front.additional_fields') as $theme_setting)

                                            @switch($theme_setting['type'])
                                                @case('editor')
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>{{ $theme_setting['title'] }}</label>
                                                                <textarea name="{{ $theme_setting['key'] }}" rows="5" class="form-control">{{ option($theme_setting['key']) }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @break

                                            @endswitch

                                        @endforeach

                                    </div>

                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                        <button type="submit" class="btn btn-primary glow">ذخیره تغییرات</button>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- users edit ends -->

        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ v_asset('back/app-assets/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>

    <script src="{{ v_asset('back/assets/js/pages/settings/themeSettings.js', 3) }}"></script>

@endpush
