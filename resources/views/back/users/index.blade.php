@extends('back.layouts.master')

@section('content')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb no-border">
                                    <li class="breadcrumb-item">مدیریت
                                    </li>
                                    <li class="breadcrumb-item">مدیریت کاربران
                                    </li>
                                    <li class="breadcrumb-item active">لیست کاربران
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="card">
                    <div class="card-header filter-card">
                        <h4 class="card-title">فیلتر کردن</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse {{ request()->except('page') ? 'show' : '' }}">
                        <div class="card-body">
                            <div class="users-list-filter">
                                <form id="filter-comments-form" method="GET"
                                      action="{{ route('admin.users.index') }}">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>نام</label>
                                            <fieldset class="form-group">
                                                <input class="form-control" name="fullname" value="{{ request('fullname') }}">
                                            </fieldset>
                                        </div>

                                        <div class="col-md-3">
                                            <label>نام کاربری</label>
                                            <fieldset class="form-group">
                                                <input class="form-control" name="username" value="{{ request('username') }}">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-3">
                                            <label>ایمیل</label>
                                            <fieldset class="form-group">
                                                <input class="form-control" name="email" value="{{ request('email') }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label>مرتب سازی</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" name="ordering">
                                                    <option value="latest" {{ request('ordering') == 'latest' ? 'selected' : '' }}>
                                                        جدیدترین
                                                    </option>
                                                    <option value="oldest" {{ request('ordering') == 'oldest' ? 'selected' : '' }}>
                                                        قدیمی ترین
                                                    </option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-3">
                                            <label>تعداد در صفحه</label>
                                            <fieldset class="form-group">
                                                <select class="form-control" name="paginate">
                                                    <option value="10" {{ request('paginate') == '10' ? 'selected' : '' }}>
                                                        10
                                                    </option>
                                                    <option value="20" {{ request('paginate') == '20' ? 'selected' : '' }}>
                                                        20
                                                    </option>
                                                    <option value="50" {{ request('paginate') == '50' ? 'selected' : '' }}>
                                                        50
                                                    </option>
                                                    <option value="all" {{ request('paginate') == 'all' ? 'selected' : '' }}>
                                                        همه
                                                    </option>
                                                </select>
                                            </fieldset>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <button type="submit"
                                                    class="btn btn-outline-success square  mb-1 waves-effect waves-light">
                                                فیلتر کردن
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                @if($users->count())
                    <section id="description" class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست کاربران</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a class="btn btn-outline-primary waves-effect waves-light excel-export" href="{{ route('admin.users.export') }}"><i class="fa fa-file-excel-o"></i> خروجی اکسل</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content" id="main-card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">تصویر</th>
                                                <th>نام </th>
                                                <th>نام کاربری</th>
                                                <th class="text-center">عملیات</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr id="user-{{ $user->id }}-tr">
                                                    <td class="text-center">
                                                        <img class="post-thumb"
                                                             src="{{ $user->imageUrl }}"
                                                             alt="image">
                                                    </td>

                                                    <td>{{  $user->fullname }}</td>
                                                    <td>{{  $user->username }}</td>

                                                    <td class="text-center">
                                                        @can('users.view')
                                                            <a href="{{ route('admin.users.show', ['user' => $user]) }}" class="btn btn-success mr-1 waves-effect waves-light">مشاهده</a>
                                                        @endcan

                                                        @can('users.update')

                                                            @if ($user->id != auth()->user()->id)
                                                                <a href="{{ route('admin.users.edit', ['user' => $user]) }}" class="btn btn-warning mr-1 waves-effect waves-light">ویرایش</a>
                                                            @else
                                                                <button type="button" class="btn btn-warning mr-1 waves-effect waves-light" disabled>ویرایش</button>
                                                            @endif

                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                @else
                    <section class="card">
                        <div class="card-header">
                            <h4 class="card-title">لیست کاربران</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="card-text">
                                    <p>چیزی برای نمایش وجود ندارد!</p>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif

                {{ $users->links() }}

            </div>
        </div>
    </div>


@endsection
