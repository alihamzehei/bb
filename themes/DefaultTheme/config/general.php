<?php

return [
    'sliderGroups' => [
        [
            'group' => '1',
            'name'  => 'اسلایدر اصلی',
            'width' => 1400,
            'height' => 500,
            'count' => 2,
            'size'  => '500 * 1400'
        ],
        [
            'group' => '3',
            'name'  => 'اسلایدر حالت موبایل',
            'width' => 658,
            'height' => 436,
            'count' => 2,
            'size'  => '436 * 658'
        ],
        [
            'group' => '2',
            'name' => 'اسلایدر لوگو همکاران',
            'width' => 100,
            'height' => 100,
            'count' => 3,
            'size' => '100 * 100'
        ],
    ],

    'bannerGroups' => [
        [
            'group' => '1',
            'name'  => 'بنر دوتایی',
            'width' => 820,
            'height' => 300,
            'count' => 2,
            'size'  => '300 * 820'
        ],
        [
            'group' => '2',
            'name' => 'بنر کنار اسلایدر اصلی',
            'width' => 272,
            'height' => 406,
            'count' => 1,
            'size' => '406 * 272'
        ],
    ],

    'linkGroups' => [
        [
            'name'  => 'گروه اول',
        ],
        [
            'name'  => 'گروه دوم',
        ],
        [
            'name'  => 'گروه سوم',
        ],
    ],

    'errors' => [
        '404' => 'front::errors.404'
    ],

    'pages' => [
        'login'    => 'front::auth.login',
        'register' => 'front::auth.register',
    ],

    'asset_path' => 'themes/defaultTheme/',
    'mainfest_path' => 'themes/defaultTheme',
    'demo' => [
        'image' => 'demo/preview.png',
        'name'  => 'قالب پیش فرض',
        'description' => 'قالب پیش فرض اسکریپت فروشگاهی ایده آل شاپ'
    ],

    'socials' => [
        [
            'name' => 'تلگرام',
            'key' => 'social_telegram',
            'icon' => 'fa fa-telegram',
        ],
        [
            'name' => 'اینستاگرام',
            'key' => 'social_instagram',
            'icon' => 'feather icon-instagram',
        ],
        [
            'name' => 'واتساپ',
            'key' => 'social_whatsapp',
            'icon' => 'fa fa-whatsapp',
        ],
    ]
];
