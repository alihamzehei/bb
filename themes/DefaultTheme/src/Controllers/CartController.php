<?php

namespace Themes\DefaultTheme\src\Controllers;

use App\Models\Cart;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function show()
    {
        return view('front::cart');
    }

    public function store(Product $product, Request $request)
    {
        $request->validate([
            'quantity' => 'required|numeric|min:1',
        ]);

        // create or get user cart
        if (auth()->check()) {
            $user = auth()->user();
            $cart = $user->cart;

            if (!$cart) {
                $cart = Cart::create([
                    'user_id' => $user->id,
                ]);
            }
        } else {
            $cart_id = Cookie::get('cart_id');

            if (!$cart_id || !($cart = Cart::find($cart_id)) || $cart->user_id != null) {
                $cart = Cart::create([
                    'user_id' => null,
                ]);
            }

            Cookie::queue(Cookie::make('cart_id', $cart->id));
        }

        if ($request->groups) {
            $price = $product->getPriceWithAttributes($request->groups);
        } else {
            $price = $product->prices()->first();
        }

        if (!$price) {
            return response(['status' => 'error', 'message' => 'موجودی محصول کافی نمی باشد']);
        }

        $cart_product = $cart->products()->where('product_id', $product->id)->where('price_id', $price->id)->first();

        if (!$cart_product) {

            if ($price->cart_min !== null && $price->cart_min > $request->quantity) {
                return response(['status' => 'error', 'message' => 'لطفا تعداد بیشتر از یا مساوی ' . $price->cart_min . ' انتخاب کنید.']);
            }

            if ($price->stock < $request->quantity || ($price->cart_max !== null && $price->cart_max < $request->quantity)) {
                return response(['status' => 'error', 'message' => 'موجودی محصول کافی نمی باشد']);
            }

            $cart->products()->attach([
                $product->id => [
                    'product_id' => $product->id,
                    'quantity'   => $request->quantity,
                    'price_id'   => $price->id
                ],
            ]);
        } else {

            if (($price->cart_min !== null && $price->cart_min > $request->quantity + $cart_product->pivot->quantity)) {
                return response(['status' => 'error', 'message' => 'لطفا تعداد بیشتر از یا مساوی ' . $price->cart_min . ' انتخاب کنید.']);
            }

            if (($cart_product->pivot->quantity + $request->quantity) < $price->stock && ($price->cart_max !== null ? ($cart_product->pivot->quantity + $request->quantity) <= $price->cart_max : true)) {
                $cart->products()->where('product_id', $product->id)->where('price_id', $price->id)->update([
                    'quantity'   => $cart_product->pivot->quantity + $request->quantity,
                ]);
            } else {
                // price stock is low
                return response(['status' => 'error', 'message' => 'موجودی محصول کافی نمی باشد']);
            }
        }



        return response(['status' => 'success', 'cart' => view('front::partials.cart')->with('render_cart', $cart)->render()]);
    }

    public function update(Request $request)
    {
        $cart = get_cart();

        $errors = [];

        foreach ($cart->products as $product) {

            $price = $product->prices()->find($product->pivot->price_id);

            if ($request->input('product-' . $product->pivot->id) > $price->stock || ($price->cart_max !== null ? $request->input('product-' . $product->pivot->id) > $price->cart_max : false)) {
                $errors[] = 'موجودی محصول "' . $product->title . ' ' . $price->getAttributesName() . '" کافی نیست.';
            }

            if ($product->type == 'physical' && ($price->cart_min !== null ? $request->input('product-' . $product->pivot->id) < $price->cart_min : false)) {
                $errors[] = 'حداقل تعداد برای محصول "' . $product->title . '"' . '، "' . $price->cart_min . '" میباشد';
            }

        }

        if (!empty($errors)) {
            return response(['errors' => $errors], 406);
        }

        foreach ($cart->products as $product) {
            $quantity = $request->input('product-' . $product->pivot->id);

            if ($quantity === '0') {
                DB::table('cart_product')->where('cart_id', $cart->id)->where('id', $product->pivot->id)->delete();
            } else if (intval($quantity) && $product->type == 'physical') {
                DB::table('cart_product')->where('cart_id', $cart->id)->where('id', $product->pivot->id)->update([
                    'quantity' => $quantity,
                ]);
            }
        }

        return response('success');
    }

    public function destroy($id)
    {
        $cart = get_cart();

        if ($cart) {
            DB::table('cart_product')->where('cart_id', $cart->id)->where('id', $id)->delete();
        }

        return redirect('/cart');
    }
}
