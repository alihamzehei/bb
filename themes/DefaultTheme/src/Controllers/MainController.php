<?php

namespace Themes\DefaultTheme\src\Controllers;

use App\Models\Banner;
use App\Models\Cart;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Product;
use App\Models\Province;
use App\Models\Slider;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $latest_products    = Product::published()
            ->available()
            ->latest()
            ->take(6)
            ->get();

        $special_products   = Product::published()
            ->available()
            ->where('special', true)
            ->take(6)
            ->get();

        $best_sell_products = Product::published()
            ->available()
            ->orderBy('sell')
            ->take(8)
            ->get();

        $discount_products  = Product::published()
            ->available()
            ->discount()
            ->take(8)
            ->get();

        $latest_posts       = Post::latest()->take(8)->get();

        $main_sliders     = Slider::where('group', 1)->where('published', true)->orderBy('ordering')->get();
        $mobile_sliders   = Slider::where('group', 3)->where('published', true)->orderBy('ordering')->get();
        $sliders_group_2  = Slider::where('group', 2)->where('published', true)->orderBy('ordering')->get();
        $banners_group_1  = Banner::where('group', 1)->where('published', true)->orderBy('ordering')->get();
        $banners_group_2  = Banner::where('group', 2)->where('published', true)->orderBy('ordering')->first();

        $categories       = Category::where('type', 'productcat')->where('image', '!=', null)->orderBy('ordering')->take(10)->get();


        return
            view('front::index', compact(
                'latest_products',
                'special_products',
                'best_sell_products',
                'discount_products',
                'main_sliders',
                'mobile_sliders',
                'sliders_group_2',
                'latest_posts',
                'banners_group_1',
                'banners_group_2',
                'categories'
            ));
    }

    public function checkout()
    {
        $cart = auth()->user()->cart;

        if (!$cart || !$cart->products->count() || !check_cart_quantity()) {
            return redirect('/cart');
        }

        $provinces = Province::all();

        return view('front::checkout', compact('provinces'));
    }

    public function getPrices(Request $request)
    {
        if ($request->city_id) {
            $request->validate([
                'city_id' => 'required|exists:cities,id',
            ]);
        }

        return view('front::partials.checkout-sidebar', ['city_id' => $request->city_id]);
    }

    public function captcha()
    {
        return response(['captcha' => captcha_src('flat')]);
    }
}
