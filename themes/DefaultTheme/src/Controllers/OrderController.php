<?php

namespace Themes\DefaultTheme\src\Controllers;

use App\Events\OrderCreated;
use App\Events\OrderPaid;
use App\Http\Controllers\Controller;
use App\Jobs\CancelOrder;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index()
    {
        $orders = auth()->user()->orders()->latest()->paginate(10);

        return view('front::user.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        return view('front::user.orders.show', compact('order'));
    }

    public function store(Request $request)
    {
        $user = auth()->user();

        $cart = $user->cart;

        if (!$cart || !$cart->products->count() || !check_cart_quantity()) {
            return redirect('/cart');
        }

        $this->validate($request, [
            'name'        => 'required|string',
            'mobile'      => 'required|string|regex:/(09)[0-9]{9}/|digits:11',
            'province_id' => 'required|exists:provinces,id',
            'city_id'     => 'required|exists:cities,id',
            'postal_code' => 'required|string',
            'gateway'     => 'required|in:mellat,payir',
            'address'     => 'required|string|max:300',
            'description' => 'nullable|string|max:1000',
        ]);

        $order = Order::create([
            'user_id'       => $user->id,
            'name'          => $request->name,
            'mobile'        => $request->mobile,
            'province_id'   => $request->province_id,
            'city_id'       => $request->city_id,
            'postal_code'   => $request->postal_code,
            'gateway'       => $request->gateway,
            'address'       => $request->address,
            'description'   => $request->description,
            'shipping_cost' => $cart->shipping_cost($request->city_id, true),
            'price'         => $cart->finalPrice($request->city_id),
            'status'        => 'unpaid',

        ]);

        //add cart products to order
        foreach ($cart->products as $product) {

            $price = $product->pivot->price_id ? $product->prices()->find($product->pivot->price_id) : null;

            if (!$price) {
                $order->items()->create([
                    'product_id'  => $product->id,
                    'title'       => $product->title,
                    'price'       => $product->discountPrice,
                    'quantity'    => $product->pivot->quantity,
                    'discount'    => $product->discount,
                ]);
            } else {
                $order->items()->create([
                    'product_id'  => $product->id,
                    'title'       => $product->title,
                    'price'       => get_discount_price($price->price, $price->discount),
                    'quantity'    => $product->pivot->quantity,
                    'discount'    => $price->discount,
                    'price_id'    => $product->pivot->price_id,
                ]);
            }

            $price->update([
                'stock' => $price->stock - $product->pivot->quantity
            ]);

        }

        $cart->delete();

        // cancel order after $hour hours
        $hour = option('order_cancel', 1);
        CancelOrder::dispatch($order)->delay(now()->addHours($hour));

        event(new OrderCreated($order));

        return $this->pay($order);
    }

    public function pay(Order $order)
    {
        if ($order->status != 'unpaid') {
            return redirect()->route('front.orders.show', ['order' => $order])->with('error', 'سفارش شما لغو شده است یا قبلا پرداخت کرده اید');
        }

        try {

            $gateway = \Larabookir\Gateway\Gateway::make($order->gateway);

            $gateway->price($order->price * 10)
                ->ready();

            $refId =  $gateway->refId(); // شماره ارجاع بانک
            $transID = $gateway->transactionId(); // شماره تراکنش

            $order->transactions()->create([
                'status'       => false,
                'amount'       => $order->price,
                'factorNumber' => $order->id,
                'mobile'       => auth()->user()->username,
                'description'  => 'تراکنش ایجاد شد برای درگاه ' . $order->gateway,
                'transID'      => $transID,
                'token'        => $refId,
                'user_id'      => auth()->user()->id,
            ]);

            session(['order_id' => $order->id]);

            return $gateway->redirect();
        } catch (\Exception $e) {
            return redirect()->route('front.orders.show', ['order' => $order])->with('transaction-error', $e->getMessage())->with('order_id', $order->id);
        }
    }

    public function verify()
    {
        $order = Order::findOrFail(session('order_id'));

        try {

            $gateway = \Larabookir\Gateway\Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();

            // تراکنش با موفقیت سمت بانک تایید گردید
            // در این مرحله عملیات خرید کاربر را تکمیل میکنیم

            $order->transactions()->updateOrCreate(
                [
                    'token'        => $refId,
                ],
                [
                    'status'       => true,
                    'amount'       => $order->price,
                    'factorNumber' => $order->id,
                    'mobile'       => $order->mobile,
                    'cardNumber'   => $cardNumber,
                    'traceNumber'  => $trackingCode,
                    'message'      => 'پرداخت موفق با درگاه ' . $order->gateway,
                    'user_id'      => auth()->user()->id,
                ]
            );

            $order->update([
                'status' => 'paid',
            ]);

            event(new OrderPaid($order));

            return redirect()->route('front.orders.show', ['order' => $order])->with('message', 'ok');
        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {

            // تراکنش قبلا سمت بانک تاییده شده است و
            // کاربر احتمالا صفحه را مجددا رفرش کرده است
            // لذا تنها فاکتور خرید قبل را مجدد به کاربر نمایش میدهیم

            return redirect()->route('front.orders.show', ['order' => $order])->with('error', $e->getMessage());
        } catch (\Exception $e) {
            // نمایش خطای بانک
            return redirect()->route('front.orders.show', ['order' => $order])->with('transaction-error', $e->getMessage());
        }
    }
}
