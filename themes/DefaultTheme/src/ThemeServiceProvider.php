<?php

namespace Themes\DefaultTheme\src;

use Illuminate\Support\ServiceProvider;
use App\Models\Category;
use App\Models\LinkGroup;
use App\Models\Menu;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // set config file
        if ($this->app['config']->get('front') === null) {
            $this->app['config']->set('front', require __DIR__ . '/../config/general.php');
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // load routes

        Route::group([
            'middleware' => ['web'],
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        });

        // load views
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'front');

        // share with views
        if (!$this->app->runningInConsole()) {
            $this->viewComposer();
        }

        // publish assets
        $this->publishes([__DIR__ . '/resources/assets' => public_path('themes/defaultTheme')], 'DefaultTheme');
    }

    private function viewComposer()
    {
        // SHARE WITH SPECIFIC VIEW


        view()->composer(['front::partials.footer'], function ($view) {

            $footer_links     = LinkGroup::all();

            $view->with('footer_links', $footer_links);
        });

        view()->composer(['front::partials.menu.menu', 'front::partials.mobile-menu.menu'], function ($view) {

            $productcats = Category::where('type', 'productcat')->whereNull('category_id')->orderBy('ordering')->get();
            $postcats    = Category::where('type', 'postcat')->whereNull('category_id')->orderBy('ordering')->get();
            $menus       = Menu::whereNull('menu_id')->orderBy('ordering')->get();

            $view->with(compact('productcats', 'postcats', 'menus'));
        });

        view()->composer(['front::posts.partials.sidebar'], function ($view) {

            $latest_posts = Post::where('published', true)->latest()->take(6)->get();

            $view->with(compact('latest_posts'));
        });

        view()->composer(['front::user.layouts.master'], function ($view) {

            $user = auth()->user();
            $random_products = Product::where('published', true)->inRandomOrder()->limit(10)->get();

            $view->with(compact('user', 'random_products'));
        });

        view()->composer(['front::partials.cart', 'front::partials.checkout-sidebar', 'front::checkout', 'front::cart'], function ($view) {
            $cart = get_cart();
            $view->with('cart', $cart);
        });
    }
}
