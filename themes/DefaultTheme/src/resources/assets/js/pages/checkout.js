jQuery('#checkout-form').validate({
    rules: {
        'name': {
            required: true,
        },
        'mobile': {
            required: true,
            regex: "(09)[0-9]{9}"
        },
        'postal_code': {
            required: true,
        },
        'province_id': {
            required: true,
        },
        'city_id': {
            required: true,
        },
        'address': {
            required: true,
            maxlength: 300,
        },
        'description': {
            maxlength: 1000,
        },
        'description': {
            maxlength: 1000,
        },

    },
});


$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "لطفا یک مقدار معتبر وارد کنید"
);

$('#city').change(function() {

    var city_id = $(this).val();

    $.ajax({
        url: '/checkout-prices',
        type: 'GET',
        data: {
            city_id: city_id
        },
        success: function(data) {
            $("#checkout-sidebar").replaceWith(data);

            if ($('.container .sticky-sidebar').length) {
                $('.container .sticky-sidebar').theiaStickySidebar();
            }
        },
    });
});