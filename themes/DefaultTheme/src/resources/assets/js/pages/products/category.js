$('#productse-filter-form').on('submit', function(e) {

    e.preventDefault();

    var form = $(this);
    var queryString = new URLSearchParams(new FormData(this)).toString()

    $.ajax({
        type: 'GET',
        url: form.attr('action') + '?' + queryString,
        success: function(data) {
            var content = $(data).find('#category-products-div').html();
            $("#category-products-div").html(content);

            $('html, body').animate({
                scrollTop: $('#category-products-div').offset().top - 20
            }, 1000);

        },
        beforeSend: function(xhr) {
            block("#category-products-div");
        },
        complete: function() {
            unblock("#category-products-div");
        },
    });
});

$('.attribute_group_input').on('change', function() {
    if ($(this).prop('checked')) {
        $('.in_stock').prop('checked', true);
    }
});

$(document).ready(function() {
    var nonLinearStepSlider = document.getElementById('slider-non-linear-step');

    noUiSlider.create(nonLinearStepSlider, {
        start: [selected_min_price, selected_max_price],
        connect: true,
        direction: 'rtl',
        format: wNumb({
            decimals: 0,
            thousand: ','
        }),
        range: {
            'min': [products_min_price],
            'max': [products_max_price]
        }
    });
    var nonLinearStepSliderValueElement = document.getElementById('slider-non-linear-step-value');

    nonLinearStepSlider.noUiSlider.on('update', function(values) {

        $('input[name="min_price"]').val(values[0].replaceAll(',', ''));
        $('input[name="max_price"]').val(values[1].replaceAll(',', ''));

        nonLinearStepSliderValueElement.innerHTML = values.join(' - ');
    });

    nonLinearStepSlider.noUiSlider.on('change', function(values) {
        $('.in_stock').prop('checked', true);
        $('.price_filter').prop('checked', true);
    });
});