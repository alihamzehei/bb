$(document).on('click', '.add-to-cart', function() {

    var btn = this;
    var groups = [];

    $('.product-info-block input:checked').each(function(index, el) {
        groups.push($(el).val());
    });

    $.ajax({
        type: 'POST',
        url: BASE_URL + '/cart/' + $(btn).data('product'),
        data: {
            quantity: $('#cart-quantity').val(),
            groups: groups
        },
        success: function(data) {

            if (data.status == 'success') {
                Swal.fire({
                    type: 'success',
                    title: 'با موفقیت اضافه شد',
                    text: 'محصول مورد نظر با موفقیت به سبد خرید شما اضافه شد برای رزرو محصول سفارش خود را نهایی کنید.',
                    confirmButtonText: 'باشه',
                    footer: '<h5><a href="/cart">مشاهده سبد خرید</a></h5>'
                });

                $('#cart-list-item').replaceWith(data.cart);
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'خطا',
                    text: data.message,
                    confirmButtonText: 'باشه',
                    footer: '<h5><a href="/cart">مشاهده سبد خرید</a></h5>'
                });
            }
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            block(btn);
        },
        complete: function() {
            unblock(btn);
        }
    });
});

$('#stock_notify_btn').click(function() {
    var btn = this;

    if ($(btn).data('user')) {
        sendStockNotify();
    } else {
        $('#modal-stock-notify').modal('show');
    }

});

function sendStockNotify() {

    var btn = $('#stock_notify_btn');

    if ($(btn).data('user')) {
        var data = {
            product_id: $(btn).data('product'),
        }
    } else {
        var data = {
            product_id: $(btn).data('product'),
            name: $('#stock-name').val(),
            mobile: $('#stock-mobile').val(),
        }
    }

    $.ajax({
        type: 'POST',
        url: BASE_URL + '/stock-notify',
        data: data,
        success: function(data) {
            toastr.success('نام شما در لیست اطلاع از موجودی این محصول قرار گرفت.', '', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            block(btn);
        },
        complete: function() {
            unblock(btn);
        }
    });
}

$('#sendStockNotifyBtn').click(sendStockNotify);

// product prices js codes

$(document).on('click', '.product-attribute', function() {

    var input = $(this).find('input');

    if (input.is(':checked')) {
        return;
    }

    setTimeout(() => {
        var product = input.data('product');
        var groups = [];

        $('.product-info-block input:checked').each(function(index, el) {
            groups.push($(el).val());
        });

        $.ajax({
            type: 'GET',
            url: BASE_URL + '/product/' + product + '/prices',
            data: {
                groups: groups
            },
            success: function(data) {

                setTimeout(() => {
                    $('.product-info-block').replaceWith(data);
                }, 200);
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                block(".product-info");
            },
            complete: function() {
                unblock(".product-info");
            }
        });
    }, 50);
});

//-------------------------- Add to favorites
$(document).on('click', '#add-to-favorites', function() {
    var btn = this;

    $.ajax({
        type: 'POST',
        url: $(btn).data('action'),
        data: {
            product_id: $(btn).data('product'),
        },
        success: function(data) {
            toastr.success('با موفقیت انجام شد', '', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            if (data.action == 'create') {
                $(btn).addClass("favorites");
                $(btn).parent().find('span').text('حذف از علاقمندی ها');
            } else {
                $(btn).removeClass("favorites");
                $(btn).parent().find('span').text('افزودن به علاقمندی ها');
            }
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            block('#add-to-favorites');
        },
        complete: function() {
            unblock('#add-to-favorites');
        }
    });
});