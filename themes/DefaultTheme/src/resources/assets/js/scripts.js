function block(el) {

    var block_ele = $(el);

    // Block Element
    block_ele.block({
        message: '<div class="mdi mdi-refresh icon-spin text-primary"></div>',
        overlayCSS: {
            backgroundColor: "#fff",
            cursor: "wait"
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: "none"
        }
    });
}

function unblock(el) {
    $(el).unblock();
}

$.ajaxSetup({
    error: function(data) {

        if (data.status == 403) {
            toastr.error('اجازه ی دسترسی ندارید', 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;

        } else if (!data.responseJSON.errors) {
            toastr.error('خطایی رخ داده است', 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });
            return;
        }

        for (var key in data.responseJSON.errors) {
            // skip loop if the property is from prototype
            if (!data.responseJSON.errors.hasOwnProperty(key)) continue;

            var obj = data.responseJSON.errors[key];
            for (var prop in obj) {
                // skip loop if the property is from prototype
                if (!obj.hasOwnProperty(prop)) continue;

                toastr.error(obj[prop], 'خطا', { positionClass: 'toast-bottom-left', containerId: 'toast-bottom-left' });

            }
        }

    },
});

$('.cart_quantity').on('click keyup keydown keypress focus', function(e) {
    e.preventDefault();
})

$('.remove_from_cart').click(function(e) {
    e.preventDefault();

    var id = $(this).data('product');

    $('#delete-form').attr('action', '/cart/' + id);
    jQuery('#delete-modal').modal('show');
});


$(document).on('click', '#update-cart, #checkout-link', function() {

    if (!$("#cart-update-form").length) {
        return;
    }

    var btn = this;

    var formData = new FormData(document.getElementById('cart-update-form'));

    $.ajax({
        url: '/cart',
        type: 'POST',
        data: formData,
        success: function(data) {
            if ($(btn).attr('id') == 'update-cart') {
                window.location.href = "/cart";
            } else {
                window.location.href = "/checkout";
            }
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
            block(btn);
            $('#cart-errors').hide();
            $('#cart-errors').find('p').remove();
        },
        complete: function() {
            unblock(btn);
        },
        error: function(request, status, error) {
            var errors = request.responseJSON.errors;
            if (errors) {
                errors.forEach(function(error) {
                    $('#cart-errors').prepend('<p>' + error + '</p>')
                });

                $('#cart-errors').show();

                $('html, body').animate({
                    scrollTop: $('body').offset().top
                }, 500);
            } else {
                alert('خطایی رخ داده است.');
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$(document).on('click', '#checkout-link', function() {
    $('#checkout-form').trigger('submit');
});

$('#province').change(function() {

    $('#city').empty();
    $('#city').append('<option value="">انتخاب کنید</option>');
    $('#city').trigger('change');
    $('.custom-select-ui select').niceSelect('update');

    if (!$(this).val()) {
        return;
    }

    var id = $(this).find(":selected").val();

    $.ajax({
        type: 'get',
        url: '/province/get-cities',
        data: { id: id },
        success: function(data) {
            $(data).each(function() {
                $('#city').append('<option value="' + $(this)[0].id + '">' + $(this)[0].name + '</option>')
            });

            $('.custom-select-ui select').niceSelect('update');
        },
        beforeSend: function() {
            //
        },
    });
});


// **************  search
$("header.main-header .search-area form.search input").keyup(delay(function(e) {
    var q = $(this).val();
    q = $.trim(q);

    if (!q) {
        $("header.main-header .search-area form.search .search-result").removeClass('open');
        $("header.main-header .search-area form.search .close-search-result").removeClass('show');
        return;
    }

    $.ajax({
        url: '/search',
        type: 'POST',
        data: {
            q: q
        },
        success: function(data) {
            $("header.main-header .search-area form.search .search-result").removeClass('open');
            $("header.main-header .search-area form.search .search-result ul").empty();
            $("header.main-header .search-area form.search .close-search-result").removeClass('show');

            if (data.length) {

                $(data).each(function(index, el) {
                    $('header.main-header .search-area form.search .search-result ul').append('<li><a href="' + el.link + '">' + el.title + '</a></li>')
                });

                // Otherwise show it
                $("header.main-header .search-area form.search .search-result").addClass('open');
                $("header.main-header .search-area form.search .close-search-result").addClass('show');
            }
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
        },
        error: function() {
            //
        }
    });
}, 300));


$("header.main-header .search-area form.search .close-search-result").on('click', function() {
    $(this).removeClass('show');
    $("header.main-header .search-area form.search .search-result").removeClass('open');
});

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
            callback.apply(context, args);
        }, ms || 0);
    };
}

$('img.captcha').on('click', function() {
    $.ajax({
        url: BASE_URL + "/get-new-captcha",
        type: 'GET',
        data: {},
        success: function(data) {
            $('img.captcha').attr('src', data.captcha);
        },
    });
});

setInterval(() => {
    $(window).lazyLoadXT();
}, 1500);