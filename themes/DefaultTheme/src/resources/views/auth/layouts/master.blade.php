<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#f7858d">
    <meta name="msapplication-navbutton-color" content="#f7858d">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f7858d">
    <title>
        @isset($title)
            {{ $title }} |
        @endisset

        {{ option('info_site_title', 'ایده آل') }}
    </title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/bootstrap.min.css') }}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'js/plugins/toastr/toastr.css') }}">

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/materialdesignicons.min.css') }}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/main.css') }}">
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/colors/default.css') }}" id="colorswitch">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ option('info_icon', asset(config('front.asset_path') . 'images/favicon-32x32.png')) }}">

</head>

<body>

<div class="wrapper">

    <!-- Start mini-header -->
    <header class="mini-header pt-4 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="logo-area-mini-header">
                        <a href="/">
                            <img src="{{ option('info_logo', asset(config('front.asset_path') . 'img/logo.png')) }}" alt="{{ option('info_site_title', 'ایده آل') }}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End mini-header -->

    @yield('content')

    <!-- Start mini-footer -->
    <footer class="mini-footer dt-sl">
        <div class="container main-container">
            <div class="row">

                <div class="col-12 mt-2 mb-3">
                    <div class="footer-light-text">
                         کلیه حقوق این سایت متعلق به ({{ option('info_site_title', 'ایده آل') }})
                          می‌باشد
                    </div>
                </div>
                <div class="col-12 text-center">
                    <div class="copy-right-mini-footer">
                        طراحی و توسعه توسط <a href="https://www.idealwebsaz.com/">ایده آل</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End mini-footer -->

</div>

<!-- Core JS Files -->
<script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/vendor/popper.min.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/vendor/bootstrap.min.js') }}"></script>
<!-- Plugins -->
<script src="{{ asset(config('front.asset_path') . 'js/vendor/ResizeSensor.min.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/plugins/jquery.blockUI.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/plugins/sweetalert2.all.min.js') }}"></script>

<script src="{{ asset(config('front.asset_path') . 'js/scripts.js') }}"></script>

<script src="{{ asset(config('front.asset_path') . 'js/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset(config('front.asset_path') . 'js/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>


@stack('scripts')
</body>

</html>
