@extends('front::layouts.master')

@section('content')


    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            @if($cart && $cart->products()->count())

                @if(!check_cart_quantity())
                    <div class="alert alert-danger" role="alert">
                        موجودی برخی از محصولات اضافه شده به سبد خرید به اتمام رسیده و یا کمتر از تعداد اضافه شده به سبد خرید است،<br> لطفا سبد خریدتان را ویرایش کنید.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span class="icon-close" aria-hidden="true"></span>
                        </button>
                    </div>
                @endif

                <div id="cart-errors" class="alert alert-danger" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span class="icon-close" aria-hidden="true"></span>
                    </button>
                </div>

                <div class="row">

                    <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 mb-2 px-0">
                        <nav class="tab-cart-page">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                                role="tab" aria-controls="nav-home" aria-selected="true">سبد خرید<span
                                            class="count-cart">{{ $cart->quantity }}</span></a>
                            </div>
                        </nav>
                    </div>
                    <div class="col-12">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                aria-labelledby="nav-home-tab">
                                <div class="row">
                                    <div class="col-xl-9 col-lg-8 col-12 px-0">
                                        <div class="table-responsive checkout-content dt-sl">
                                            <div class="checkout-header checkout-header--express">
                                                <span class="checkout-header-title">({{ $cart->products->count() }} کالا)</span>
                                            </div>

                                            <table class="table table-cart">
                                                <form id="cart-update-form" action="/cart" method="POST">
                                                    @method('put')
                                                    @csrf
                                                    <tbody>
                                                        @foreach($cart->products as $product)
                                                            <tr class="checkout-item">
                                                                <td>
                                                                    <a href="{{ route('front.products.show', ['product' => $product]) }}">
                                                                        <img class="cart-product-image" src="{{ $product->image ? $product->image : '/empty.jpg' }}" alt="{{ $product->title }}">
                                                                    </a>
                                                                    <button class="checkout-btn-remove remove_from_cart" data-product="{{ $product->pivot->id }}">&times;</button>
                                                                </td>
                                                                <td class="text-right">
                                                                    <a href="{{ route('front.products.show', ['product' => $product]) }}">
                                                                        <h3 class="checkout-title">{{ $product->title }}</h3>


                                                                        @php
                                                                            $price_to_stock = $product->prices()->find($product->pivot->price_id);
                                                                        @endphp

                                                                        @if($product->pivot->quantity > $price_to_stock->stock || ($price_to_stock->cart_max !== null ? $product->pivot->quantity > $price_to_stock->cart_max : false))
                                                                            <small class="text-danger">موجودی کافی نیست</small>
                                                                        @endif

                                                                        @if($price_to_stock->cart_min !== null ? $product->pivot->quantity < $price_to_stock->cart_min : false)
                                                                            <small class="text-danger">حداقل تعداد فروش برای این محصول {{ $price_to_stock->cart_min }} میباشد</small>
                                                                        @endif

                                                                    </a>

                                                                    @php
                                                                        $p_price = $product->prices()->find($product->pivot->price_id);
                                                                    @endphp

                                                                    @foreach ($p_price->get_attributes as $attribute)

                                                                        @if ($attribute->group->type == 'color')
                                                                            <div class="checkout-variant checkout-variant--color">
                                                                                <span class="checkout-variant-title">{{ $attribute->group->name }} :</span>
                                                                                <span class="checkout-variant-value">
                                                                                    {{ $attribute->name }}
                                                                                    <div class="checkout-variant-shape"
                                                                                        style="background-color:{{ $attribute->value }}"></div>
                                                                                </span>
                                                                            </div>
                                                                        @else
                                                                            <p class="checkout-guarantee">{{ $attribute->group->name }} : {{ $attribute->name }}</p>
                                                                        @endif

                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    <p class="mb-0">تعداد</p>
                                                                    <div class="number-input">
                                                                        <button
                                                                               type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"></button>
                                                                        <input class="quantity cart_quantity" min="{{ cart_min($p_price) }}" name="product-{{ $product->pivot->id }}" max="{{ cart_max($p_price) }}" value="{{ $product->pivot->quantity }}" type="number" required>
                                                                        <button
                                                                               type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                                                                class="plus"></button>
                                                                    </div>
                                                                </td>
                                                                <td>

                                                                    @php
                                                                        $cart_product_price = $product->prices()->find($product->pivot->price_id);
                                                                    @endphp

                                                                    <strong>{{ number_format(get_discount_price($cart_product_price->price, $cart_product_price->discount) * $product->pivot->quantity) }} تومان</strong>
                                                                    @if($cart_product_price->discount)
                                                                        <del class="text-danger">{{ number_format($cart_product_price->price * $product->pivot->quantity) }} تومان</del>
                                                                    @endif

                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </form>
                                            </table>

                                        </div>
                                        <div class="text-left">
                                            <button id="update-cart" type="button" class="btn btn-light">بروزرسانی سبد خرید</button>
                                        </div>
                                    </div>
                                    @include('front::partials.checkout-sidebar')
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-12">
                        <div class="dt sl dt-sn pt-3 pb-5">
                            <div class="cart-page cart-empty">
                                <div class="circle-box-icon">
                                    <i class="mdi mdi-cart-remove"></i>
                                </div>
                                <p class="cart-empty-title">سبد خرید شما خالیست!</p>
                                <p class="pb-3">می‌توانید برای مشاهده محصولات بیشتر به صفحه اصلی بروید:</p>

                                <a href="/" class="btn-primary-cm">برو  به صفحه اصلی</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </main>
    <!-- End main-content -->

    <!-- Start Modal location new -->
    <div class="modal fade" id="delete-modal" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">
                        <i class="now-ui-icons location_pin"></i>
                        پاک کردن این محصول
                    </h5>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <p>آیا تمایل به حذف این محصول از سبدخرید دارید؟</p>

                            <div class="form-ui dt-sl">
                                <form id="delete-form" action="#" method="POST">
                                    <div class="modal-body text-center">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-md">حذف</button>
                                        <button class="btn btn-light" data-dismiss="modal">لغو</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal location new -->


@endsection
