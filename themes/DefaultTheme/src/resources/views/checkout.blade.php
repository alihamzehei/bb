@extends('front::layouts.master')
@push('styles')

    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/nice-select.css') }}">


@endpush

@section('wrapper-classes', 'shopping-page')

@section('content')
    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <form id="checkout-form" action="{{ route('front.orders.store') }}" class="setting_form" method="POST">
                @csrf
                <div class="row">

                        <div class="cart-page-content col-xl-9 col-lg-8 col-12 px-0">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            @endif

                            <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 px-res-1">
                                <h2>انتخاب آدرس تحویل سفارش</h2>
                            </div>
                            <section class="page-content dt-sl">
                                <div class="form-ui dt-sl pt-4 pb-4 checkout-div">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    نام و نام خانوادگی <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <input class="input-ui pr-2 text-right"
                                                        type="text"
                                                        name="name" value="{{ old('name') ?: auth()->user()->fullname }}"
                                                        placeholder="نام خود را وارد نمایید">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    شماره موبایل <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <input
                                                        class="input-ui pl-2 dir-ltr text-left"
                                                        type="text"
                                                        name="mobile"  value="{{ old('mobile') ?: auth()->user()->username }}"
                                                        placeholder="09xxxxxxxxx">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    استان <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <div class="custom-select-ui">
                                                    <select class="right" name="province_id" id="province">
                                                        <option value="">انتخاب کنید</option>

                                                        @foreach ($provinces as $province)
                                                            <option value="{{ $province->id }}"
                                                                @if(auth()->user()->address && auth()->user()->address->province->id == $province->id) selected @endif>
                                                                {{ $province->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    شهر <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <div class="custom-select-ui ">
                                                    <select class="right" name="city_id" id="city">
                                                        <option value="">انتخاب کنید</option>

                                                        @if(auth()->user()->address)

                                                            @foreach (auth()->user()->address->province->cities as $city)
                                                                <option value="{{ $city->id }}" @if($city->id == auth()->user()->address->city->id) selected @endif>{{ $city->name }}</option>
                                                            @endforeach

                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    آدرس پستی <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <textarea
                                                        class="input-ui pr-2 text-right"
                                                        name="address"
                                                        placeholder=" آدرس تحویل گیرنده را وارد نمایید">{{ user_address('address') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                توضیحات سفارش
                                                </h4>
                                            </div>
                                            <div class="form-row">
                                                <textarea
                                                        class="input-ui pr-2 text-right"
                                                        name="description">{{ old('description') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <div class="form-row-title">
                                                <h4>
                                                    کد پستی <sup class="text-danger">*</sup>
                                                </h4>
                                            </div>
                                            <div class="form-row form-group">
                                                <input
                                                        class="input-ui pl-2 dir-ltr text-left placeholder-right"
                                                        type="text"
                                                        name="postal_code" value="{{ user_address('postal_code') }}"
                                                        placeholder=" کد پستی را بدون خط تیره بنویسید">
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-2">
                                            <div class="checkout-invoice">
                                                <div class="checkout-invoice-headline">
                                                    <div class="custom-control custom-checkbox pr-0 form-group">
                                                        <input id="agreement" name="agreement" type="checkbox" class="custom-control-input" required>
                                                        <label for="agreement" class="custom-control-label">با قوانین سایت موافقم</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 px-res-1">
                                    <h2>انتخاب نحوه ارسال</h2>
                                </div>
                                <div class="dt-sn pt-3 pb-3">
                                    <div class="checkout-shipment mb-4">
                                        <div class="custom-control custom-radio pl-0 pr-3">
                                            <input type="radio" class="custom-control-input" name="radio1" id="radio1"
                                                value="option1" checked>
                                            <label for="radio1" class="custom-control-label">
                                                عادی
                                            </label>
                                        </div>

                                    </div>

                                </div>

                                <section class="page-content dt-sl pt-2">
                                    <div class="section-title text-sm-title title-wide no-after-title-wide mb-0 px-res-1">
                                        <h2>انتخاب شیوه پرداخت</h2>
                                    </div>
                                    <div class="dt-sn pt-3 pb-3 mb-4">
                                        <div class="checkout-pack">
                                            <div class="row">
                                                <div class="checkout-time-table checkout-time-table-time">
                                                    @if (option('gateway_payir_status') == 'on')
                                                        <div class="col-12">
                                                            <div class="radio-box custom-control custom-radio pl-0 pr-3">
                                                                <input type="radio" class="custom-control-input" name="gateway" id="payir" value="payir" checked>
                                                                <label for="payir" class="custom-control-label">
                                                                    <i class="mdi mdi-credit-card-outline checkout-additional-options-checkbox-image"></i>
                                                                    <div class="content-box">
                                                                        <div class="checkout-time-table-title-bar checkout-time-table-title-bar-city">
                                                                            پرداخت اینترنتی pay.ir
                                                                        </div>
                                                                        <ul class="checkout-time-table-subtitle-bar">
                                                                            <li>
                                                                                آنلاین با تمامی کارت‌های بانکی
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if (option('gateway_mellat_status') == 'on')
                                                        <div class="col-12">
                                                            <div class="radio-box custom-control custom-radio pl-0 pr-3">
                                                                <input type="radio" class="custom-control-input" name="gateway" id="mellat" value="mellat" checked>
                                                                <label for="mellat" class="custom-control-label">
                                                                    <i class="mdi mdi-credit-card-outline checkout-additional-options-checkbox-image"></i>
                                                                    <div class="content-box">
                                                                        <div class="checkout-time-table-title-bar checkout-time-table-title-bar-city">
                                                                            پرداخت اینترنتی به پرداخت ملت
                                                                        </div>
                                                                        <ul class="checkout-time-table-subtitle-bar">
                                                                            <li>
                                                                                آنلاین با تمامی کارت‌های بانکی
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </section>

                                <div class="mt-5">
                                    <a href="{{ route('front.cart') }}" class="float-right border-bottom-dt"><i
                                                class="mdi mdi-chevron-double-right"></i>بازگشت به سبد خرید</a>

                                </div>
                            </section>
                        </div>


                    @include('front::partials.checkout-sidebar')

                </div>
            </form>

        </div>
    </main>
    <!-- End main-content -->
@endsection

@push('scripts')
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/wNumb.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/ResizeSensor.min.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>

    <script src="{{ asset(config('front.asset_path') . 'js/pages/checkout.js?v=1') }}"></script>

@endpush
