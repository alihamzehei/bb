@extends('front::layouts.master')

@push('meta')
    <meta name="description" content="{{ option('info_short_description') }}">
    <meta name="keywords" content="{{ option('info_tags') }}">
@endpush

@section('content')

    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        {{--  <div class="trasparent-background"></div>  --}}
        <div class="container main-container">

            <!-- Start Main-Slider -->
            <div class="row mb-5">
                <aside class="sidebar col-xl-2 col-lg-3 col-12 order-2 order-lg-1 pl-0 hidden-md">
                    <!-- Start banner -->
                    @if ($banners_group_2)
                        <div class="sidebar-inner dt-sl">
                            <div class="sidebar-banner">
                                <a href="{{ $banners_group_2->link ?: '#' }}" target="_top">
                                    <img data-src="{{ $banners_group_2->image }}" style="width: 100%;height: 329px;"
                                        alt="top banner">
                                </a>
                            </div>
                        </div>
                    @endif
                    <!-- End banner -->
                </aside>
                <div class="col-xl-10 col-lg-8 col-12 order-1 order-lg-2">
                    @include('front::partials.sliders')
                </div>
            </div>
            <!-- End Main-Slider -->

            <!-- Start discount products -->
            @if ($discount_products->count())
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <section class="slider-section dt-sl mb-5">
                            <div class="row mb-3">
                                <div class="col-12">
                                    <div class="section-title text-sm-title title-wide no-after-title-wide">
                                        <h2>محصولات تخفیف دار</h2>
                                        <a href="{{ route('front.products.discount') }}">مشاهده همه</a>
                                    </div>
                                </div>

                                <div class="col-12 px-res-0">
                                    <div class="product-carousel carousel-md owl-carousel owl-theme">
                                        @foreach ($discount_products as $product)
                                            @include('front::partials.product-block')
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            @endif
            <!-- End discount products -->

            <!-- Start special products -->
            @if ($special_products->count())
                <section class="slider-section mb-5 amazing-section" style="background: #ef394e">
                    <div class="container main-container">
                        <div class="row mb-3">
                            <div class="col-lg-2 col-xs-12">
                                <div class="item">
                                    <div class="amazing-product text-center pt-5">
                                        <a href="{{ route('front.products.specials') }}">
                                            <img data-src="{{ asset(config('front.asset_path') . "img/theme/amazing-1.png") }}" alt="special products">
                                        </a>
                                        <a href="{{ route('front.products.specials') }}" class="view-all-amazing-btn">
                                            مشاهده همه
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10 col-xs-12">
                                <div class="product-carousel carousel-lg owl-carousel owl-theme">
                                    @foreach ($special_products as $product)
                                        @include('front::partials.product-block-2')
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <!-- End special products -->

            <!-- Start Banner -->
            @if ($banners_group_1->count())
                <div class="row mt-3 mb-5">
                    @foreach ($banners_group_1 as $banner)
                        <div class="col-sm-6 col-12 mb-2">
                            <div class="widget-banner">
                                <a href="{{ $banner->link }}">
                                    <img data-src="{{ $banner->image }}" alt="banner">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
            <!-- End Banner -->

            <!-- Start Category-Section -->
            @if ($categories->count())
                <div class="row mt-3 mb-3">
                    <div class="col-12">
                        <div class="category-section dt-sn dt-sl">
                            <div class="category-section-title dt-sl">
                                <h3>دسته بندی محصولات</h3>
                            </div>
                            <div class="category-section-slider dt-sl">
                                <div class="category-slider owl-carousel">
                                    @foreach ($categories as $category)
                                        <div class="item">
                                            <a href="{{ $category->link }}" class="promotion-category">
                                                <img data-src="{{ $category->image }}" alt="{{ $category->title }}">
                                                <h4 class="promotion-category-name">{{ $category->title }}</h4>
                                                <h6 class="promotion-category-quantity">{{ $category->products->count() }}
                                                    کالا</h6>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <!-- End Category-Section -->

            <!-- Start latest products -->
            @if ($latest_products->count())
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <section class="slider-section dt-sl mb-5">
                            <div class="row mb-3">
                                <div class="col-12">
                                    <div class="section-title text-sm-title title-wide no-after-title-wide">
                                        <h2>جدید ترین محصولات</h2>
                                    </div>
                                </div>

                                <div class="col-12 px-res-0">
                                    <div class="product-carousel carousel-md owl-carousel owl-theme">
                                        @foreach ($latest_products as $product)
                                            @include('front::partials.product-block')
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            @endif
            <!-- End latest products -->

            <!-- Start best sells -->
            @if ($best_sell_products->count())
                <section class="slider-section mb-5 amazing-section" style="background: #304ffe">
                    <div class="container main-container">
                        <div class="row mb-3">

                            <div class="col-lg-2 col-xs-12">
                                <div class="item">
                                    <div class="amazing-product text-center pt-5">
                                        <img data-src="{{ asset(config('front.asset_path') . "img/theme/amazing-2.png") }}" alt="best sell products">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10 col-xs-12">
                                <div class="product-carousel carousel-lg owl-carousel owl-theme">
                                    @foreach ($best_sell_products as $product)
                                        @include('front::partials.product-block-2')
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            @endif
            <!-- End best sells -->

            <!-- Start Partners -->
            @if ($sliders_group_2->count())
                <section class="slider-section dt-sl mb-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="brand-slider carousel-sm owl-carousel owl-theme">
                                @foreach ($sliders_group_2 as $slider)
                                    <div class="item">
                                        <img data-src="{{ $slider->image }}" class="img-fluid" alt="{{ $slider->title }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <!-- End Partners -->

            <!-- services -->
            <div class="bk-icons-footer">
                @if (request()->is('/'))
                    <div class="footer-services">
                        <div class="row">
                            <div class="service-item col">
                                <img data-src="{{ asset(config('front.asset_path') . "img/svg/delivery.svg") }}">
                                <p>ارسال سریع</p>
                            </div>
                            <div class="service-item col">
                                <img data-src="{{ asset(config('front.asset_path') . "img/svg/contact-us.svg") }}">
                                <p>پشتیبانی قوی</p>
                            </div>
                            <div class="service-item col">
                                <img data-src="{{ asset(config('front.asset_path') . "img/svg/origin-guarantee.svg") }}">
                                <p>کالای اصل</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <!-- End services -->

        </div>

    </main>
    <!-- End main-content -->

@endsection
