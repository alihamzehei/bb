<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @stack('meta')

    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ option('info_icon', asset(config('front.asset_path') . 'images/favicon-32x32.png')) }}">

    <title>
        @isset($title)
            {{ $title }} |
        @endisset

        {{ option('info_site_title', 'ایده آل') }}
    </title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/materialdesignicons.min.css') }}">

    @stack('befor-styles')

    @if (config('app.debug'))
        <!-- inject:css -->
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/bootstrap-rtl.min.css') }}">
        <!-- Plugins -->
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/jquery.horizontalmenu.css') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/fancybox.min.css') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . "js/plugins/toastr/toastr.css") }}">

        <!-- Main CSS File -->
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/main.css?v=2') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/styles.css?v=2') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/custom.css?v=1') }}">
        <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/colors/default.css') }}">
        <!-- endinject -->
    @else
        <!-- All Css Files -->
        <link rel="stylesheet" href="{{ mix('css/all.css', config('front.mainfest_path')) }}">
    @endif

    @stack('styles')
</head>

<body>
    <div class="wrapper @yield('wrapper-classes')">


        <!-- Start header -->
        <header class="main-header dt-sl">

            <!-- Start topbar -->
            <div class="container main-container">
                <div class="topbar dt-sl">
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-6">
                            <div class="logo-area float-right">
                                <a href="{{ route('front.index') }}">
                                    <img data-src="{{ option('info_logo', asset(config('front.asset_path') . 'img/logo.png')) }}" alt="{{ option('info_site_title', 'ایده آل') }}">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5 hidden-sm">
                            <div class="search-area dt-sl">
                                <form id="search-form" action="{{ route('front.products.search') }}" class="search">
                                    <input type="text" name="q" value="{{ request('q') }}" id="search-input" autocomplete="off" placeholder="نام کالای مورد نظر خود را جستجو کنید…">
                                    <button type="submit"><img data-src="{{ asset(config('front.asset_path') . 'img/theme/search.png') }}" alt="search button"></button>
                                    <button id="close-search-result" class="close-search-result" type="button"><i class="mdi mdi-close"></i></button>
                                    <div class="search-result" id="search-result">
                                        <ul></ul>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4 col-6 topbar-left">
                            @include('front::partials.user-menu')
                        </div>
                    </div>
                </div>
            </div>
            <!-- End topbar -->

            <!-- Start bottom-header -->
            <div class="bottom-header dt-sl mb-sm-bottom-header">
                <div class="container main-container">
                    <!-- Start Main-Menu -->
                    @include('front::partials.menu.menu')
                    <!-- End Main-Menu -->
                </div>
            </div>
            <!-- End bottom-header -->
        </header>
        <!-- End header -->


        @yield('content')

        @include('front::partials.footer')
    </div>



    @if (config('app.debug'))
        <!-- Core JS Files -->
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/popper.min.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/bootstrap.min.js') }}"></script>
        <!-- Plugins -->
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/owl.carousel.min.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery.horizontalmenu.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/theia-sticky-sidebar.min.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery.fancybox.min.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . 'js/vendor/jquery.lazyloadxt.min.js') }}"></script>

        <script src="{{ asset(config('front.asset_path') . "js/plugins/jquery.blockUI.js") }}"></script>
        <script src="{{ asset(config('front.asset_path') . "js/plugins/sweetalert2.all.min.js") }}"></script>
        <script src="{{ asset(config('front.asset_path') . "js/plugins/toastr/toastr.min.js") }}"></script>

        <!-- Main JS File -->
        <script src="{{ asset(config('front.asset_path') . 'js/main.js') }}"></script>
        <script src="{{ asset(config('front.asset_path') . "js/scripts.js?v=1") }}"></script>
    @else
        <!-- All JS Files -->
        <script src="{{ mix('js/all.js', config('front.mainfest_path')) }}"></script>
    @endif

    <script>
        $.lazyLoadXT.onload.addClass = 'animated fadeIn';
        var BASE_URL = "{{ route('front.index') }}";
    </script>

    @stack('scripts')

    {!! option('info_scripts') !!}
    <!-- endinject -->
</body>

</html>
