@if($main_sliders->count())
<!-- Start main-slider -->
<section id="main-slider" class="main-slider carousel slide carousel-fade card hidden-sm"
         data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach($main_sliders as $slider)
            <li data-target="#main-slider" data-slide-to="{{ $loop->index }}" class="{{ ($loop->index == 0) ? 'active' : '' }}"></li>
        @endforeach
    </ol>
    <div class="carousel-inner">
        @foreach($main_sliders as $slider)
            <div class="carousel-item {{ ($loop->index == 0) ? 'active' : '' }}">
                <a class="main-slider-slide" href="{{ $slider->link }}"
                   style="background-image: url({{ $slider->image }})">
                </a>
            </div>
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">
        <i class="mdi mdi-chevron-right"></i>
    </a>
    <a class="carousel-control-next" href="#main-slider" data-slide="next">
        <i class="mdi mdi-chevron-left"></i>
    </a>
</section>

@endif

@if($mobile_sliders->count())

    <section id="main-slider-res"
            class="main-slider carousel slide carousel-fade card d-none show-sm" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach($mobile_sliders as $slider)
                <li data-target="#main-slider-res" data-slide-to="{{ $loop->index }}" class="{{ ($loop->index == 0) ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach($mobile_sliders as $slider)
                <div class="carousel-item {{ ($loop->index == 0) ? 'active' : '' }}">
                    <a class="main-slider-slide" href="{{ $slider->link }}">
                        <img data-src="{{ $slider->image }}" alt="{{ $slider->title }}"
                            class="img-fluid">
                    </a>
                </div>
            @endforeach
            
        </div>
        <a class="carousel-control-prev" href="#main-slider-res" role="button" data-slide="prev">
            <i class="mdi mdi-chevron-right"></i>
        </a>
        <a class="carousel-control-next" href="#main-slider-res" data-slide="next">
            <i class="mdi mdi-chevron-left"></i>
        </a>
    </section>
    <!-- End main-slider -->
@endif

