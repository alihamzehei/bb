@extends('front::layouts.master', ['title' => $post->title])

@section('content')

    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <!-- Start title - breadcrumb -->
            <div class="title-breadcrumb-special dt-sl">
                <div class="breadcrumb dt-sl">
                    <nav>
                        <a href="/">خانه</a>
                        <a href="{{ route('front.posts.index') }}">وبلاگ</a>
                        <a href="#">{{ $post->title }}</a>
                    </nav>
                </div>
                <div class="title-page dt-sl pb-3">
                    <h1>{{ $post->title }}</h1>
                </div>

            </div>
            <!-- End title - breadcrumb -->

            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12 col-12 mb-3">
                    <div class="content-page">
                        <div class="content-desc dt-sn dt-sl">
                            <header class="entry-header dt-sl mb-3">
                                <div class="post-meta date">
                                    <i class="mdi mdi-calendar-month"></i>{{ verta($post->created_at)->format('%d %B %Y') }}
                                </div>

                                @if($post->category)
                                    <div class="post-meta category">
                                        <i class="mdi mdi-folder"></i>

                                        <a href="{{ route('front.posts.category', ['category' => $post->category]) }}">{{ $post->category->title }}</a>
                                    </div>
                                @endif
                                <div class="post-meta category">
                                    <i class="mdi mdi-eye"></i>
                                    {{ $post->view }} بازدید
                                </div>
                            </header>

                            @if($post->image)
                                <div class="post-thumbnail dt-sl">
                                    <img data-src="{{ $post->image }}" alt="{{ $post->title }}">
                                </div>
                            @endif

                            <div class="row pt-4">
                                <div class="col-12">
                                    {!! $post->content !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3">
                            @include('front::components.comments', ['model' => $post, 'route_link' => route('front.post.comments', ['post' => $post]) ])
                        </div>
                    </div>

                </div>

                @include('front::posts.partials.sidebar')

            </div>

        </div>
    </main>
    <!-- End main-content -->
@endsection

@push('scripts')
    <script src="{{ asset(config('front.asset_path') . 'js/pages/comments.js') }}"></script>
@endpush
