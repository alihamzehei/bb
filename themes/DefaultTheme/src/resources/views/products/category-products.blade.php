@extends('front::layouts.master', ['title' => $category->title])

@push('meta')
    <meta property="og:title" content="{{ $category->meta_title ?: $category->title }}" />
    <meta property="og:url" content="{{ route('front.products.category-products', ['category' => $category]) }}" />
    <meta name="description" content="{{ $category->meta_description ?: $category->description }}">
    <meta name="keywords" content="{{ $category->getTags }}">
@endpush

@push('befor-styles')
    <link rel="stylesheet" href="{{ asset(config('front.asset_path') . 'css/vendor/nouislider.min.css') }}">
@endpush

@section('content')

    <!-- Start main-content -->
    <main class="main-content dt-sl mt-4 mb-3">
        <div class="container main-container">

            <div class="row">
                <!-- Start Content -->
                <div class="title-breadcrumb-special dt-sl mb-3">
                    <div class="breadcrumb dt-sl">
                        <nav>
                            <a href="/">خانه</a>
                            <a href="{{ route('front.products.index') }}">محصولات</a>

                            @foreach ($category->parents() as $parent)
                                <a href="{{ route('front.products.category', ['category' => $parent]) }}">{{ $parent->title }}</a>
                            @endforeach
                            <span>{{ $category->title }}</span>
                        </nav>
                    </div>
                </div>

                @if ($category->description)

                    <div class="dt-sl dt-sn search-amazing-tab mb-3 mx-3" >
                        <div class="row">

                            <div class="col-md-12 p-md-5 category-background" style="background-image: url({{ asset($category->background_image) }});">
                                {!! $category->description !!}
                            </div>
                        </div>
                    </div>

                @endif
            </div>
            <div class="row">
                @if ($category->getFilter())
                    @include('front::products.partials.category-filters')
                @endif

                <div id="category-products-div" class="{{ $category->getFilter() ? 'col-lg-9' : 'col-lg-12' }} col-md-12 col-sm-12">
                    @if($products->count())
                        <div class="dt-sl dt-sn px-0 search-amazing-tab">
                            <div class="row mb-3 mx-0 px-res-0">
                                @foreach($products as $product)

                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 px-10 mb-1 px-res-0">
                                        @include('front::products.partials.product-card', ['product' => $product])
                                    </div>

                                @endforeach
                            </div>

                            {{ $products->appends(request()->all())->links('front::components.paginate') }}
                        </div>
                    @else
                        @include('front::partials.empty')
                    @endif
                </div>
            </div>

        </div>
    </main>
    <!-- End main-content -->


@endsection

@push('scripts')

    <script>
        var selected_min_price = {{ request('min_price') ?: $min_price }};
        var selected_max_price = {{ request('max_price') ?: $max_price }};
        var products_min_price = {{ $min_price }};
        var products_max_price = {{ $max_price }};
    </script>

    <script src="{{ asset(config('front.asset_path') . 'js/pages/products/category.js?v=2') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/nouislider.min.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/wNumb.js') }}"></script>
    <script src="{{ asset(config('front.asset_path') . 'js/vendor/ResizeSensor.min.js') }}"></script>
@endpush
