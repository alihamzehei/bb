<?php

use Themes\DefaultTheme\src\Controllers\MainController;
use Themes\DefaultTheme\src\Controllers\PostController;
use Themes\DefaultTheme\src\Controllers\ProductController;
use Themes\DefaultTheme\src\Controllers\CartController;
use Themes\DefaultTheme\src\Controllers\StockNotifyController;
use Themes\DefaultTheme\src\Controllers\PageController;
use Themes\DefaultTheme\src\Controllers\OrderController;
use Themes\DefaultTheme\src\Controllers\UserController;
use Themes\DefaultTheme\src\Controllers\SitemapController;
use Themes\DefaultTheme\src\Controllers\ContactController;
use Themes\DefaultTheme\src\Controllers\FavoriteController;

// ------------------ Front Part Routes

Route::group(['as' => 'front.'], function () {
    // ------------------ MainController
    Route::get('/', [MainController::class, 'index'])->name('index');
    Route::get('/get-new-captcha', [MainController::class, 'captcha']);

    // ------------------ posts
    Route::resource('posts', PostController::class)->only(['index', 'show']);
    Route::get('posts/category/{category}', [PostController::class, 'category'])->name('posts.category');

    // ------------------ products
    Route::resource('products', ProductController::class)->only(['show', 'index']);
    Route::get('products/category/{category}', [ProductController::class, 'category'])->name('products.category');
    Route::get('products/category-products/{category}', [ProductController::class, 'categoryProducts'])->name('products.category-products');
    Route::get('products/category-specials/{category}', [ProductController::class, 'categorySpecials'])->name('products.category-specials');
    Route::get('search', [ProductController::class, 'search'])->name('products.search');
    Route::post('search', [ProductController::class, 'ajax_search'])->name('products.ajax_search');
    Route::get('product/specials', [ProductController::class, 'specials'])->name('products.specials');
    Route::get('product/discount', [ProductController::class, 'discount'])->name('products.discount');
    Route::get('product/{product}/prices', [ProductController::class, 'prices'])->name('products.prices');
    Route::get('product/compare/{product1}/{product2?}/{product3?}', [ProductController::class, 'compare'])->name('products.compare');
    Route::post('product/compare', [ProductController::class, 'similarCompare'])->name('products.similar-compare');


    // ------------------ cart
    Route::get('cart', [CartController::class, 'show'])->name('cart');
    Route::post('cart/{product}', [CartController::class, 'store']);
    Route::put('cart', [CartController::class, 'update']);
    Route::delete('cart/{id}', [CartController::class, 'destroy']);

    Route::post('stock-notify', [StockNotifyController::class, 'store']);


    // ------------------ pages
    Route::get('pages/{page}', [PageController::class, 'show'])->name('pages.show');

    // ------------------ sitemap
    Route::get('sitemap', [SitemapController::class, 'index']);
    Route::get('sitemap-posts', [SitemapController::class, 'posts']);
    Route::get('sitemap-pages', [SitemapController::class, 'pages']);
    Route::get('sitemap-products', [SitemapController::class, 'products']);

    // ------------------ contacts
    Route::resource('contact', ContactController::class)->only(['index', 'store']);

    // ------------------ authentication required
    Route::group(['middleware' => 'auth'], function () {

        // ------------------ MainController
        Route::get('checkout', [MainController::class, 'checkout'])->name('checkout');
        Route::get('checkout-prices', [MainController::class, 'getPrices'])->name('checkout.prices');

        // ------------------ orders
        Route::resource('orders', OrderController::class);
        Route::get('orders/pay/{order}', [OrderController::class, 'pay'])->name('orders.pay');
        Route::get('orders/payment/callback', [OrderController::class, 'verify'])->name('orders.verify');
        Route::get('orders/payment/mellat/callback', [OrderController::class, 'verify']);
        Route::post('orders/payment/mellat/callback', [OrderController::class, 'verify'])->name('orders.verify.mellat');

        // ------------------ user
        Route::get('user/profile', [UserController::class, 'profile'])->name('user.profile');
        Route::get('user/comments', [UserController::class, 'comments'])->name('user.comments');
        Route::get('user/edit-profile', [UserController::class, 'editProfile'])->name('user.profile.edit');
        Route::put('user/profile', [UserController::class, 'update_profile'])->name('user.profile.update');
        Route::get('user/change-password', [UserController::class, 'changePassword'])->name('user.password');
        Route::put('user/change-password', [UserController::class, 'updatePassword'])->name('user.password.update');

        // ------------------ products
        Route::get('products/{product}/download', [ProductController::class, 'download'])->name('products.download');
        Route::post('products/{product}/comments', [ProductController::class, 'comments'])->name('product.comments');


        // ------------------ posts
        Route::post('posts/{post}/comments', [PostController::class, 'comments'])->name('post.comments');

        // ------------------ orders
        Route::resource('favorites', FavoriteController::class)->only(['index', 'store', 'destroy']);

    });
});
